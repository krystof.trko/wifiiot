# Systém pro správu a administraci IoT Wi-Fi

### Client

- JavaScript
  - [Ionic](https://ionicframework.com/)
  - [Vue.js 3](https://vuejs.org/)
  - [Vue Router](https://router.vuejs.org/)
  - [Pinia](https://pinia.vuejs.org)
  - [Apollo Client](https://github.com/apollographql/apollo-client)
  - [graphql](https://github.com/graphql/graphql-js)
  - [graphql-tag](https://github.com/ngogiaphat/GraphQLTag)
  - [ionicons](https://ionic.io/ionicons)
  - [apollo-upload-client](https://github.com/jaydenseric/apollo-upload-client)
  - [vuelidate](https://vuelidate-next.netlify.app)
- Android
  - [Capacitor](https://capacitorjs.com/)
- Grafika
  - [Iot icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/iot)
  - [404 animace](https://codepen.io/trajektorijus/pen/mdeBYrX)
  - [Simplex noise](https://github.com/jwagner/simplex-noise.js)

### Server - api

- [PHP >=8.3](https://www.php.net/)
  - bcmath
  - pdo_mysql
  - zip
  - intl
  - gd --with-freetype --with-jpeg
  - Ctype, iconv, PCRE, Session, SimpleXML, Tokenizer
- [Symfony](https://symfony.com/)
- [Doctrine](https://www.doctrine-project.org/)
- [GraphQLBundle](https://github.com/overblog/GraphQLBundle)
- [JWTRefreshTokenBundle](https://github.com/markitosgv/JWTRefreshTokenBundle)
- [NelmioCorsBundle](https://github.com/nelmio/NelmioCorsBundle)

### Běhové prostředí

- [Debian Bookworm](https://www.debian.org/)
  - zip
  - libfreetype6-dev
  - libzip-dev
  - libjpeg-dev
  - libpng-dev
  - libicu-dev
- [MariaDB 11.3](https://mariadb.org/)

### Vývoj

- Nástroje
  - [Git](https://git-scm.com/)
  - [Docker](https://www.docker.com/)
  - [Adminer](https://www.adminer.org/)
  - [Voyager](https://graphql-kit.com/graphql-voyager/)
  - [GraphQL IDE Monorepo](https://github.com/graphql/graphiql)
  - [Postman](https://www.postman.com/)
  - [Visual Studio Code](https://code.visualstudio.com/)
    - devsense.composer-php-vscode
    - devsense.intelli-php-vscode
    - devsense.phptools-vscode
    - devsense.profiler-php-vscode
    - graphql.vscode-graphql
    - graphql.vscode-graphql-syntax
    - ionic.ionic
    - ms-azuretools.vscode-docker
    - neilbrayfield.php-docblocke
    - vue.volar
- Client
  - [TypeScript](https://www.typescriptlang.org/)
  - [NPM](https://www.npmjs.com/)
  - [Vite](https://vitejs.dev/)
  - [ESLint](https://eslint.org/)
  - [terser](https://terser.org/)
  - [Vue Language Tools](https://github.com/vuejs/language-tools)
- Server
  - [Composer](https://getcomposer.org/)
  - [FakerPHP](https://fakerphp.github.io/)
  - [PHP Coding Standards Fixer](https://github.com/PHP-CS-Fixer PHP-CS-Fixer)

## Instalace pro vývoj

Nastavení cesty k Android SDK:

```bash
  export ANDROID_SDK_ROOT=~/Android/Sdk/
```

Naklonování projektu:

```bash
  git clone git@gitlab.com:krystof.trko/wifiiot.git
```

Konfigurační soubory dockeru:

```bash
cp ./docker/docker-compose.example.yml ./docker-compose.yml
```

Spuštění docker containeru a instalace závislostí:

```bash
docker-compose run wifi_iot_client_dev bash -c "npm update"
docker-compose run wifi_iot_api_dev bash -c "composer update"
docker-compose up
```

Po připojení do kontejneru wifi_iot_api_dev:

```bash
docker exec -it wifi_iot_api_dev bash
```

PHP CS

```bash
mkdir --parents tools/php-cs-fixer
composer require --working-dir=tools/php-cs-fixer friendsofphp/php-cs-fixer
```

Instalace databáze:

```bash
bin/console doctrine:migrations:migrate -n
bin/console doctrine:fixtures:load -n
```

Oprávnění, exit

```bash
  chmod -R 777 server/temp
  exit
```

## Vývoj

Připojení do server kontejneru:

```bash
  docker exec -it wifi_iot_api_dev bash
```

Připojení do client kontejneru:

```bash
  docker exec -it wifi_iot_client_dev bash
```

Kontrola a oprava kódu v wifi_iot_api_dev kontejneru:

```bash
  composer fs
  composer phpstan
```

Kontrola a oprava kódu v wifi_iot_client_dev kontejneru:

```bash
  npm run lint
```

## Instalace pro produkční nasazení

Server Ubuntu 22.04

```bash
sudo apt update
sudo apt upgrade
sudo apt dist-upgrade

reboot

sudo apt update
sudo apt upgrade

sudo apt install mariadb-server git nginx

sudo apt install ca-certificates apt-transport-https software-properties-common lsb-release -y
sudo add-apt-repository ppa:ondrej/php -y
sudo apt update

sudo apt install php8.3 php8.3-fpm php8.3-cli
sudo apt install php8.3-{cli,fpm,curl,mysqlnd,gd,opcache,zip,intl,common,bcmath,imagick,xmlrpc,readline,memcached,mbstring,apcu,xml,dom,memcache}
sudo apt install composer

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

nvm install 20
```

Deploy key pro gitlab:

```bash
ssh-keygen
more /root/.ssh/id_rsa.pub
```

Konfigurace souborů /var/www/wifiiot/server/.env

Clone a instalace npm composer

```bash
cd /var/www
rm -rf html
git clone git@gitlab.com:krystof.trko/wifiiot.git

cd /var/www/wifiiot/server
composer install

cd /var/www/wifiiot/client
npm install
npm run build

```

Instalace databáze:

```bash
mysql -u root

CREATE DATABASE wifiiot;
CREATE USER wifiiot@localhost IDENTIFIED BY password;
GRANT ALL PRIVILEGES ON *.* TO wifiiot@localhost IDENTIFIED BY 'password';
FLUSH PRIVILEGES;

cd /var/www/wifiiot/server/bin

php console doctrine:migrations:migrate -n
php console doctrine:fixtures:load -n # test data
```

Konfigurace nginx

```nginx
server {
	root  /var/www/wifiiot/client/dist;

	index index.html;
    server_name sulis198.zcu.cz; # managed by Certbot

	error_log  /var/log/nginx/wifiiot_front_error.log;
    access_log /var/log/nginx/wifiiot_front_access.log;

	gzip on;
    gzip_types text/plain text/css text/js text/xml text/javascript application/javascript application/x-javascript application/json application/xml application/rss+xml;

	add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

	charset utf-8;

	location /api {
      error_log  /var/log/nginx/wifiiot_api_error.log;
      access_log /var/log/nginx/wifiiot_api_access.log;

      alias /var/www/wifiiot/server/public;
      include fastcgi.conf;

      rewrite /api/(.*) /api/$1 break;

      fastcgi_param SCRIPT_FILENAME $document_root/index.php;
      fastcgi_param DOCUMENT_URI /index.php;
      fastcgi_param SCRIPT_NAME /index.php;
      fastcgi_param REQUEST_URI $1$is_args$query_string;
      fastcgi_pass unix:/var/run/php/php8.3-fpm.sock;
    }

    location /device_photo {
      error_log  /var/log/nginx/wifiiot_public_error.log;
      access_log /var/log/nginx/wifiiot_public_access.log;

      alias /var/www/wifiiot/server/public/device_photo;

      try_files $fastcgi_script_name =404;
    }

    location / {
        try_files $uri $uri/ /index.html;
    }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/sulis198.zcu.cz/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/sulis198.zcu.cz/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = sulis198.zcu.cz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


	listen 80 ;
	listen [::]:80 ;
    server_name sulis198.zcu.cz;
    return 404; # managed by Certbot
}
```

Let's Encrypt certifikát

```bash
sudo apt install snapd
sudo snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
```

```bash
composer install --no-dev --optimize-autoloader
npm install
npm run build
```

Klíč pro generování JWT

```bash
cd /var/www/wifiiot/server/bin
php server/bin/console lexik:jwt:generate-keypair
```

### Struktura aplikace

- Přihlásit (login)
- 404 (notFound)

- Menu - uživatel

  - Domů (home)
  - Zařízení - filtr moje/nepřiřazená (deviceList)
    - CRUD zařízení (device)
  - Skupiny - filtr moje (groupList)
    - CRUD skupina (group)
  - Aktivní přihlášení (me)
  - Odhlásit (-)
  - Vzhled (-)

- Menu - admin
  - Domů (home)
  - Zařízení - filtr moje/nepřiřazená/všechna (deviceList)
    - CRUD zařízení (device)
  - Skupiny - filtr moje/všechny (groupList)
    - CRUD skupina (group)
  - Uživatelé (userList)
    - CRUD uživatele (user)
  - Správa statických IP (staticIpList)
    - CRUD statické IP (staticIp)
  - Aktivní přihlášení (me)
  - Odhlásit (-)
  - Vzhled

License: GPL-3.0-only
