module.exports = {
    client: {
      service: {
        name: 'WiFi IoT',
        url: 'http://localhost:8102',

        headers: {
          authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTUyODI3ODcsImV4cCI6MTc0NTI4Mjc4Nywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiYWRtaW4ifQ.Uc_mQ5JZdjzd1MjQA_z4MixtqKsPbiqsYSVaZo-JfRSDpd1g8qWudcJfGcxj4f3yKaR7JU-Iu9sJmkrf-p39_fKgkDGXKOdRyLkDxEgua2pSSbsOXbNv3uTyBFsezXRXypSFn0eKJKgkZgFafmsNZ2vxRVxTfqhh3KzCbxrJvh78gVZA6VezoGogvm548XWqEIfNTy91pLkHhPFPrRpMZ8S6ZJTW0v-wMyZ1umWeysZqcFr4EosWzDyIPMAfUJUmuXlOk1yRb2FY7xITtNB-i5_2dhw1KM7brZxE-c8gIkHXmg9ZGBCCYjMsZTooIv8a_oA44mvJJ7leZbA5UXCR3w'
        },
      },
      includes: [
        'client/src/**/*.vue',
        'client/src/**/*.js',
      ],
    },
  }