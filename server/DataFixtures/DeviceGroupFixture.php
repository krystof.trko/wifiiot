<?php

namespace DataFixtures;

use App\Entity\AppUser;
use App\Entity\Device;
use App\Entity\DeviceGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class DeviceGroupFixture extends Fixture implements DependentFixtureInterface
{
    public const int AMOUNT = 15;
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::AMOUNT; ++$i) {
            $randOrder0 = rand(0, DeviceFixture::AMOUNT - 1);
            $device0 = $this->getReference(DeviceFixture::DEVICE_REFERENCE.$randOrder0);
            assert($device0 instanceof Device);

            $randOrder1 = rand(0, DeviceFixture::AMOUNT - 1);
            $device1 = $this->getReference(DeviceFixture::DEVICE_REFERENCE.$randOrder1);
            assert($device1 instanceof Device);

            $randOrder2 = rand(0, AppUserFixture::AMOUNT - 1);
            $user = $this->getReference(AppUserFixture::APP_USER_REFERENCE.$randOrder2);
            assert($user instanceof AppUser);

            $deviceGroup = new DeviceGroup(
                $this->faker->word(),
                $user,
                $this->faker->boolean(80)
            );

            $device0->deviceGroupList->add($deviceGroup);
            $device1->deviceGroupList->add($deviceGroup);

            $manager->persist($device0);
            $manager->persist($device1);
            $manager->persist($deviceGroup);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AppUserFixture::class,
            DeviceFixture::class,
        ];
    }
}
