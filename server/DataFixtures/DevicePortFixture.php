<?php

namespace DataFixtures;

use App\Entity\Device;
use App\Entity\DevicePort;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class DevicePortFixture extends Fixture implements DependentFixtureInterface
{
    public const int AMOUNT = 20;
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::AMOUNT; ++$i) {
            $randOrder = rand(0, DeviceFixture::AMOUNT - 1);
            $device = $this->getReference(DeviceFixture::DEVICE_REFERENCE.$randOrder);
            assert($device instanceof Device);

            $devicePort = new DevicePort(
                $device,
                $this->faker->unique()->numberBetween(8000, 8999),
                $this->faker->numberBetween(8000, 8999),
                $this->faker->optional(0.3)->dateTime('2050-01-01')
            );

            $manager->persist($devicePort);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            DeviceFixture::class,
        ];
    }
}
