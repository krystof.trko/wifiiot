<?php

namespace DataFixtures;

use App\Entity\AppUser;
use App\Module\AppUser\AppUserStateEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppUserFixture extends Fixture
{
    public const int AMOUNT = 50;
    public const string APP_USER_REFERENCE = 'appUser';
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $appUser = new AppUser(
            'admin',
            $this->faker->name(),
            true,
            AppUserStateEnum::ACTIVE
        );

        $manager->persist($appUser);
        $this->setReference(self::APP_USER_REFERENCE.'0', $appUser);

        $appUser = new AppUser(
            'user',
            $this->faker->name(),
            false,
            AppUserStateEnum::ACTIVE
        );

        $manager->persist($appUser);
        $this->setReference(self::APP_USER_REFERENCE.'1', $appUser);

        for ($i = 2; $i < self::AMOUNT; ++$i) {
            $appUser = new AppUser(
                $this->faker->unique()->userName(),
                $this->faker->name(),
                $this->faker->boolean(10),
                $this->randomState()
            );

            $manager->persist($appUser);
            $this->setReference(self::APP_USER_REFERENCE.$i, $appUser);
        }

        $manager->flush();
    }

    private function randomState(): AppUserStateEnum
    {
        $arr = array_column(AppUserStateEnum::cases(), 'value');

        return AppUserStateEnum::from($arr[array_rand($arr)]);
    }
}
