<?php

namespace DataFixtures;

use App\Entity\AppUser;
use App\Entity\Device;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class DeviceFixture extends Fixture implements DependentFixtureInterface
{
    public const int AMOUNT = 100;
    public const string DEVICE_REFERENCE = 'device';

    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::AMOUNT; ++$i) {
            $isKnown = $this->faker->boolean(70);

            $name = '';
            $owner = null;
            $ip = null;
            $allowed = false;

            if ($isKnown) {
                $name = $this->faker->word();

                $owner = $this->getReference(AppUserFixture::APP_USER_REFERENCE.rand(0, AppUserFixture::AMOUNT - 1));
                assert($owner instanceof AppUser);

                $ip = $this->faker->optional()->ipv4();
                $allowed = $this->faker->boolean(90);
            }

            $device = new Device(
                $name,
                $this->faker->macAddress(),
                $ip,
                $isKnown,
                $this->faker->optional()->dateTime('2050-01-01'),
                $owner,
                $this->faker->boolean(20),
                $this->faker->optional()->dateTime('2050-01-01'),
                null, // Vazba přiřazena v StaticIpFixture
                $this->faker->optional(0.2)->word(),
                $this->faker->optional(0.2)->password(),
                $allowed
            );

            $manager->persist($device);
            $this->setReference(self::DEVICE_REFERENCE.$i, $device);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AppUserFixture::class,
        ];
    }
}
