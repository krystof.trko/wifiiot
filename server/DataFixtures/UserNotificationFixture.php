<?php

namespace DataFixtures;

use App\Entity\AppUser;
use App\Entity\UserNotification;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class UserNotificationFixture extends Fixture implements DependentFixtureInterface
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < AppUserFixture::AMOUNT; ++$i) {
            $user = $this->getReference(AppUserFixture::APP_USER_REFERENCE.$i);
            assert($user instanceof AppUser);

            $userNotification = new UserNotification('Vítejte v aplikaci WiFi IoT!', $user, false);

            $manager->persist($userNotification);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AppUserFixture::class,
        ];
    }
}
