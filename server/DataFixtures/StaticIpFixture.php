<?php

namespace DataFixtures;

use App\Entity\Device;
use App\Entity\StaticIp;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class StaticIpFixture extends Fixture implements DependentFixtureInterface
{
    public const int AMOUNT = 20;
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $usedReferences = [];

        for ($i = 0; $i < self::AMOUNT; ++$i) {
            $randOrder = rand(0, DeviceFixture::AMOUNT - 1);

            if (in_array($randOrder, $usedReferences)) {
                $device = null;
            } else {
                $usedReferences[] = $randOrder;
                $device = $this->getReference(DeviceFixture::DEVICE_REFERENCE.$randOrder);
                assert($device instanceof Device);
            }

            $staticIp = new StaticIp(
                $device,
                $this->faker->optional(0.3)->dateTime('2050-01-01'),
                $this->faker->ipv4()
            );

            $manager->persist($staticIp);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            DeviceFixture::class,
        ];
    }
}
