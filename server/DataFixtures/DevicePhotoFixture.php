<?php

namespace DataFixtures;

use App\Entity\Device;
use App\Entity\DevicePhoto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class DevicePhotoFixture extends Fixture implements DependentFixtureInterface
{
    public const int AMOUNT = 50;
    private Generator $faker;

    public function __construct(private string $uploadDir)
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $this->deteleImages();

        for ($i = 0; $i < self::AMOUNT; ++$i) {
            $randOrder = rand(0, DeviceFixture::AMOUNT - 1);
            $device = $this->getReference(DeviceFixture::DEVICE_REFERENCE.$randOrder);
            assert($device instanceof Device);

            for ($ii = 0; $ii < rand(1, 3); ++$ii) {
                $devicePhoto = new DevicePhoto(
                    $device,
                    $this->downloadImg(),
                    $this->faker->numberBetween(1, 10)
                );

                $manager->persist($devicePhoto);
            }
        }

        $manager->flush();
    }

    /**
     * Smazat složku s obrázky.
     */
    private function deteleImages(): void
    {
        $files = glob($this->uploadDir.'*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    /**
     * Stáhnout náhodný obrázek.
     */
    private function downloadImg(): string
    {
        $image = file_get_contents('https://picsum.photos/512/256.jpg');

        $fileName = $this->faker->unique()->sha1().'.jpg';
        file_put_contents($this->uploadDir.$fileName, $image);

        return $fileName;
    }

    public function getDependencies()
    {
        return [
            DeviceFixture::class,
        ];
    }
}
