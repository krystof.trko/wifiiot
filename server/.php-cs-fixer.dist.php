<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude([
        'vendor',
        'var',
        'tools'
    ])
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true
    ])
    ->setFinder($finder)
;