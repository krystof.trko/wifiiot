<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240511115715 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE app_user (id INT AUTO_INCREMENT NOT NULL, roles JSON NOT NULL COMMENT \'(DC2Type:json)\', last_login_at DATETIME DEFAULT NULL, username VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, is_admin TINYINT(1) NOT NULL, state VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_IDENTIFIER_USERNAME (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, mac VARCHAR(17) NOT NULL, ip VARCHAR(15) DEFAULT NULL, is_known TINYINT(1) NOT NULL, is_known_expiration DATETIME DEFAULT NULL, has_internet_access TINYINT(1) NOT NULL, internet_access_expiration DATETIME DEFAULT NULL, wifi_name VARCHAR(255) DEFAULT NULL, wifi_pass VARCHAR(255) DEFAULT NULL, is_allowed TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_92FB68E7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_device_group (device_id INT NOT NULL, device_group_id INT NOT NULL, INDEX IDX_D930437394A4C7D4 (device_id), INDEX IDX_D930437370608067 (device_group_id), PRIMARY KEY(device_id, device_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_group (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_AB45A4A27E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_photo (id INT AUTO_INCREMENT NOT NULL, device_id INT DEFAULT NULL, filename VARCHAR(255) NOT NULL, display_order INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D232647F94A4C7D4 (device_id), UNIQUE INDEX filename (filename), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_port (id INT AUTO_INCREMENT NOT NULL, device_id INT DEFAULT NULL, hub_port INT NOT NULL, device_port INT NOT NULL, expiration DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_CAA888F694A4C7D4 (device_id), UNIQUE INDEX hubPort (hub_port), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refresh_token (id INT AUTO_INCREMENT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid DATETIME NOT NULL, UNIQUE INDEX UNIQ_C74F2195C74F2195 (refresh_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE static_ip (id INT AUTO_INCREMENT NOT NULL, device_id INT DEFAULT NULL, expiration DATETIME DEFAULT NULL, ip VARCHAR(15) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8424F9DC94A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68E7E3C61F9 FOREIGN KEY (owner_id) REFERENCES app_user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE device_device_group ADD CONSTRAINT FK_D930437394A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_device_group ADD CONSTRAINT FK_D930437370608067 FOREIGN KEY (device_group_id) REFERENCES device_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_group ADD CONSTRAINT FK_AB45A4A27E3C61F9 FOREIGN KEY (owner_id) REFERENCES app_user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE device_photo ADD CONSTRAINT FK_D232647F94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE device_port ADD CONSTRAINT FK_CAA888F694A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE static_ip ADD CONSTRAINT FK_8424F9DC94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68E7E3C61F9');
        $this->addSql('ALTER TABLE device_device_group DROP FOREIGN KEY FK_D930437394A4C7D4');
        $this->addSql('ALTER TABLE device_device_group DROP FOREIGN KEY FK_D930437370608067');
        $this->addSql('ALTER TABLE device_group DROP FOREIGN KEY FK_AB45A4A27E3C61F9');
        $this->addSql('ALTER TABLE device_photo DROP FOREIGN KEY FK_D232647F94A4C7D4');
        $this->addSql('ALTER TABLE device_port DROP FOREIGN KEY FK_CAA888F694A4C7D4');
        $this->addSql('ALTER TABLE static_ip DROP FOREIGN KEY FK_8424F9DC94A4C7D4');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP TABLE device_device_group');
        $this->addSql('DROP TABLE device_group');
        $this->addSql('DROP TABLE device_photo');
        $this->addSql('DROP TABLE device_port');
        $this->addSql('DROP TABLE refresh_token');
        $this->addSql('DROP TABLE static_ip');
    }
}
