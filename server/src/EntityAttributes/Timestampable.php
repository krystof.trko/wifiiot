<?php

declare(strict_types=1);

namespace App\EntityAttributes;

use Doctrine\ORM\Mapping as ORM;

trait Timestampable
{
    /**
     * Vytvořeno.
     */
    #[ORM\Column()]
    public \DateTime $createdAt;

    /**
     * Upraveno.
     */
    #[ORM\Column(nullable: true)]
    public ?\DateTime $updatedAt;

    #[ORM\PrePersist]
    public function setCreatedAt(): void
    {
        $this->createdAt = new \DateTime();
    }

    #[ORM\PreUpdate]
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }
}
