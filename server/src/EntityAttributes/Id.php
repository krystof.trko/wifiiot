<?php

declare(strict_types=1);

namespace App\EntityAttributes;

use Doctrine\ORM\Mapping as ORM;

trait Id
{
    /**
     * ID.
     */
    #[ORM\Id, ORM\Column, ORM\GeneratedValue('IDENTITY')]
    public int $id;

    public function __clone()
    {
        $this->id = 0;
    }
}
