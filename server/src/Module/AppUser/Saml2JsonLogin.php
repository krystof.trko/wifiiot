<?php

namespace App\Module\AppUser;

use App\Entity\AppUser;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use OneLogin\Saml2\Auth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\JsonLoginAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\HttpUtils;

/**
 * Dědí z final class.
 *
 * @phpstan-ignore-next-line
 */
class Saml2JsonLogin extends JsonLoginAuthenticator
{
    private PropertyAccessorInterface $propertyAccessor;

    /**
     * @param UserProviderInterface<AppUser> $userProvider
     * @param string[]                       $options
     */
    public function __construct(
        HttpUtils $httpUtils,
        private UserProviderInterface $userProvider,
        AuthenticationSuccessHandler $successHandler,
        AuthenticationFailureHandler $failHandler,
        private array $options = [],
        ?PropertyAccessorInterface $propertyAccessor = null)
    {
        $optionsNew = ['check_path' => '/login', 'username_path' => 'username', 'password_path' => 'password'];
        parent::__construct($httpUtils, $userProvider, $successHandler, $failHandler, $optionsNew, $propertyAccessor);

        $this->options = $optionsNew;
        $this->propertyAccessor = $propertyAccessor ?: PropertyAccess::createPropertyAccessor();
    }

    public function authenticate(Request $request): Passport
    {
        try {
            $data = json_decode($request->getContent());

            if (!$data instanceof \stdClass) {
                throw new BadRequestHttpException('Invalid JSON.');
            }

            $credentials = $this->getCredentials($data);
        } catch (BadRequestHttpException $e) {
            $request->setRequestFormat('json');

            throw $e;
        }

        $userBadge = new UserBadge($credentials['username'], $this->userProvider->loadUserByIdentifier(...));

        $credentialChecker = new CustomCredentials([$this, 'checkCredentials'], $credentials);

        $passport = new Passport($userBadge, $credentialChecker, [new RememberMeBadge((array) $data)]);

        return $passport;
    }

    /**
     * @param string[] $credential
     */
    public function checkCredentials(array $credential, AppUser $user): bool
    {
        // Pouze aktivní uživatelé se mohou přihlásit
        if (AppUserStateEnum::ACTIVE !== $user->state) {
            return false;
        }

        $user->lastLoginAt = new \DateTime();

        return true;
        /*
        // https://shib.zcu.cz/idp/shibboleth
        // https://github.com/SAML-Toolkits/php-saml/tree/master
        // https://github.com/search?q=shib.zcu.cz&type=code&p=1
                $spBaseUrl = 'http://212.11.106.147';

                $settings = [
                    // If 'strict' is True, then the PHP Toolkit will reject unsigned
                    // or unencrypted messages if it expects them to be signed or encrypted.
                    // Also it will reject the messages if the SAML standard is not strictly
                    // followed: Destination, NameId, Conditions ... are validated too.
                    'strict' => true,

                    // Enable debug mode (to print errors).
                    'debug' => true, // TODO off

                    // Set a BaseURL to be used instead of try to guess
                    // the BaseURL of the view that process the SAML Message.
                    // Ex http://sp.example.com/
                    //    http://example.com/sp/
                    'baseurl' => null,

                    // Service Provider Data that we are deploying.
                    'sp' => [
                        // Identifier of the SP entity  (must be a URI)
                        'entityId' => $spBaseUrl,
                        // Specifies info about where and how the <AuthnResponse> message MUST be
                        // returned to the requester, in this case our SP.
                        'assertionConsumerService' => [
                            // URL Location where the <Response> from the IdP will be returned
                            'url' => $spBaseUrl,
                            // SAML protocol binding to be used when returning the <Response>
                            // message. SAML Toolkit supports this endpoint for the
                            // HTTP-POST binding only.
                            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                        ],
                        // If you need to specify requested attributes, set a
                        // attributeConsumingService. nameFormat, attributeValue and
                        // friendlyName can be omitted
                        'attributeConsumingService' => [
                            'serviceName' => 'SP test',
                            'serviceDescription' => 'Test Service',
                            'requestedAttributes' => [
                                [
                                    'name' => '',
                                    'isRequired' => false,
                                    'nameFormat' => '',
                                    'friendlyName' => '',
                                    'attributeValue' => [],
                                ],
                            ],
                        ],
                        // Specifies info about where and how the <Logout Response> message MUST be
                        // returned to the requester, in this case our SP.
                        'singleLogoutService' => [
                            // URL Location where the <Response> from the IdP will be returned
                            'url' => $spBaseUrl,
                            // SAML protocol binding to be used when returning the <Response>
                            // message. SAML Toolkit supports the HTTP-Redirect binding
                            // only for this endpoint.
                            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                        ],
                        // Specifies the constraints on the name identifier to be used to
                        // represent the requested subject.
                        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported.
                        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
                        // Usually x509cert and privateKey of the SP are provided by files placed at
                        // the certs folder. But we can also provide them with the following parameters
                        'x509cert' => 'MIICvjCCAaYCAQAwNjELMAkGA1UEBhMCQ1oxFjAUBgNVBAgMDVBsemXFiCBSZWdp
                        b24xDzANBgNVBAcMBlBpbHNlbjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
                        ggEBAMwIddBr4UZUFPBeeO4MrUnJfQeC0vjXikTH3LYQSfCSacbh5Z7GyOW47brz
                        x8rsUqvvBZ5sQX+Z2K/YKO2l+7xCNc3/GO7zscSxKZH3eyF164s9vZX+JfUzoIit
                        SAM3yxHEJcR04oPfacVgM034AKcOxlHhvgqHhJyYb5ln8FIjpeaECyqR89aPBAyf
                        jsvydc7CbygyMQQQC5nCW/Eq97KBnFBELCvhH4VYJ4izfSHu78SIhVmEDAuc3ywb
                        2AiW7I+/79UjX3N25vWcqjAHVVwB1ccqTOkGW4oxcSwGWRkPaBqfTHQhzfTnGHRs
                        yN20feZb4X19+IpqDYdx6azGwMcCAwEAAaBDMEEGCSqGSIb3DQEJDjE0MDIwDgYD
                        VR0PAQH/BAQDAgWgMCAGA1UdJQEB/wQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAN
                        BgkqhkiG9w0BAQsFAAOCAQEAejoy612pWoX27lvVDwOakJ5qkwFwfPvzKJbB/OxH
                        36/h4rLDscMLITpJQHdI9ctwuCdRMZqyfc0YZb/BU1PGpbgqSQEOp8rDB0d8tw7X
                        i3L2lFZsuYFKB0D0q0Z2tcP5CBhLfjOTdScAnvxLOwns/+xpVR/sLJi/kiehJK0I
                        FlgsE6ilh7hRMMkAqAs09ev6a/g9hF3AnU2BsvcxtwbhSZYIEeuYZLZ/VyU/SXDP
                        oDO1R7FCY2j94EVKEg6bCfGjso1m2u9eX60w6tgyim/S9O6uYDqGDDrV4ensd6LW
                        DLi4ofBd4tLFemprvxn40FtxyXwLFcLYR4AWF9RRA4QN9w==',
                        'privateKey' => 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDMCHXQa+FGVBTw
                        XnjuDK1JyX0HgtL414pEx9y2EEnwkmnG4eWexsjluO2688fK7FKr7wWebEF/mdiv
                        2Cjtpfu8QjXN/xju87HEsSmR93shdeuLPb2V/iX1M6CIrUgDN8sRxCXEdOKD32nF
                        YDNN+ACnDsZR4b4Kh4ScmG+ZZ/BSI6XmhAsqkfPWjwQMn47L8nXOwm8oMjEEEAuZ
                        wlvxKveygZxQRCwr4R+FWCeIs30h7u/EiIVZhAwLnN8sG9gIluyPv+/VI19zdub1
                        nKowB1VcAdXHKkzpBluKMXEsBlkZD2gan0x0Ic305xh0bMjdtH3mW+F9ffiKag2H
                        cemsxsDHAgMBAAECggEBAIJCJHUbaobrxzJuHeZcUYV6DUhuTSJw+OVQc6gIXiwo
                        TT+bmGz1m+yNU7i3SiZTGbSKtJufoXFFVwFsjzSpcz2NR6xr4A8tpaBdOpzl0sZZ
                        8jbDA/Zueu1AOI0Jk5/6TrrY7wOtW6+1QmGwORDd9Ayzcgl0hU9TlWYkK5tUsCi/
                        VR1N/jWwXvJmngZ4cYZkKPDd9LzZ4y2EQ2reQ1NclY8TecMhSckPzLt8fAhFpmup
                        xu/TaJj83QSP4BrAQMFs/yiOrJCK8Sz4wz7JjXML8iYG6rmV9mOk0kMKpOUorqNM
                        uQSZe9xcq7x5x3HOVzrXEEaPJMNAxzBS8EnsjhZAHAECgYEA8vz5YChyW425Qu7+
                        UY2ToDNsI38Tv7nMILLTnubfdxnwk1gFEGXaCK4HvGw5AYiGpYLJ6UlqafdplOiF
                        nJJHGL76kFbwjx22uFSgqzg8goANO90kEb1rlPM3OHiOe+UP3WAX7sL4yh+iEDK0
                        F9d9AeKwVfCynN9VIx9u5a2AjWECgYEA1vV3T9gJWAwa3epK2GrH4bcY+lcXAJDN
                        pxCgVY2sRIOPtVNSHoyZ/y3MaFdN2ZJ0m/EcvBjgntbvnWlGYELU9qH9Vq/iVQjX
                        SNGonGgeyYwEKYaOmfk3kfiZc3C3q2X0NVrFO+HDQC3uLIFqtdzqLQa1XQjLr48e
                        gXrs14jtlycCgYA+5neTicTYqF5N0lopOFH/xW/33Fx7ZBhQWppnZvJv/VoGemGN
                        NiBz4MEF6Kn1GKdqTun/ps6SMca2El8MAd2/rShlluvc0F9NZjLVP9CCI63JUlyU
                        7wjbXP5tct/HeKtbrhVj7DrE791e/hEanuCwTUNCa/WAoFuEpQ1rDTfa4QKBgCd2
                        Enwe3cBDIJ+ChlDqcV03Xqd+W2OoMYis3bI2+tvtBvGzpQykpRSaVL8G3+zEHRIP
                        lkXVj065iG1uSRQc0LmiDT7ftP4FTHg6vOhRmO1I/e6dnAoaHSB38bumgqE9azJs
                        Qm72yAA9Lk8PRwax0m+Hnr3Agpvs+x2r8lSy92LbAoGBALramT46HqEfFIQsUXQK
                        wUW50mJmSdkK4m7Awa3PsoYVrlL6RkBdlRh6kiqB3cKIFjKVzd71XVLFwQNtUaC7
                        fzB+z6TdtIctk7zyvAV2kNHSs+F33Lwkv1LiE6ev+GLIDI+CBhrlExJKIeSGc8uY
                        6cIEvnv6Hmm5yII5RZ8rX9GL',


                        // Key rollover
                        // If you plan to update the SP x509cert and privateKey
                        // you can define here the new x509cert and it will be
                        // published on the SP metadata so Identity Providers can
                        // read them and get ready for rollover.
                        //
                        // 'x509certNew' => '',
                    ],

                    // Identity Provider Data that we want connected with our SP.
                    'idp' => [
                        // Identifier of the IdP entity  (must be a URI)
                        'entityId' => 'https://shib.zcu.cz/idp/shibboleth',
                        // SSO endpoint info of the IdP. (Authentication Request protocol)
                        'singleSignOnService' => [
                            // URL Target of the IdP where the Authentication Request Message
                            // will be sent.
                            'url' => 'https://shib.zcu.cz',
                            // SAML protocol binding to be used when returning the <Response>
                            // message. SAML Toolkit supports the HTTP-Redirect binding
                            // only for this endpoint.
                            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                        ],
                        // SLO endpoint info of the IdP.
                        'singleLogoutService' => [
                            // URL Location of the IdP where SLO Request will be sent.
                            'url' => 'https://shib.zcu.cz',
                            // URL location of the IdP where the SP will send the SLO Response (ResponseLocation)
                            // if not set, url for the SLO Request will be used
                            'responseUrl' => '',
                            // SAML protocol binding to be used when returning the <Response>
                            // message. SAML Toolkit supports the HTTP-Redirect binding
                            // only for this endpoint.
                            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                        ],
                        // Public x509 certificate of the IdP
                        //   'x509cert' => '',
                        //
                         //  Instead of use the whole x509cert you can use a fingerprint in order to
                         //  validate a SAMLResponse, but we don't recommend to use that
                         //  method on production since is exploitable by a collision attack.
                         //  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it,
                         //   or add for example the -sha256 , -sha384 or -sha512 parameter)
                         //
                         //  If a fingerprint is provided, then the certFingerprintAlgorithm is required in order to
                         //  let the toolkit know which algorithm was used. Possible values: sha1, sha256, sha384 or sha512
                         //  'sha1' is the default value.
                         //
                         //  Notice that if you want to validate any SAML Message sent by the HTTP-Redirect binding, you
                         //  will need to provide the whole x509cert.
                         //
                        // 'certFingerprint' => '',
                        // 'certFingerprintAlgorithm' => 'sha1',

                        // In some scenarios the IdP uses different certificates for
                         // signing/encryption, or is under key rollover phase and
                         // more than one certificate is published on IdP metadata.
                         // In order to handle that the toolkit offers that parameter.
                         // (when used, 'x509cert' and 'certFingerprint' values are
                         // ignored).

                        'x509certMulti' => [
                            'signing' => [
                                0 => 'MIIEGDCCAoCgAwIBAgIVAOvUEYcOaRjKHXAPvdHsbdnoI6OsMA0GCSqGSIb3DQEB CwUAMBYxFDASBgNVBAMMC3NoaWIuemN1LmN6MB4XDTIwMDcwOTE1MTg1NVoXDTQw MDcwOTE1MTg1NVowFjEUMBIGA1UEAwwLc2hpYi56Y3UuY3owggGiMA0GCSqGSIb3 DQEBAQUAA4IBjwAwggGKAoIBgQDVZ805RS4QM55lykdgZKC1Sz9sM199s0bcn6Sy xSYLyugr0BKB1Xr0PbY3coC4PMEwFxcaSvq3gg8xYCWZb5QOr3pCfFY1dxkeTwnY OAXLawf+NuxYM1k/kXS4jrxaN0L0rewjL0BSq35Lf5DLHwWOhACbebB+PE335/jj S18kxvFUI/8CA+mshp+G7Mh+fXup9v9CWEEhp4F7AQpbP589D7UdhEuyTUBpj2mB VEf7zyQkdF2FmIVGk/aVeDJuJ3ngYALkpKJSNdEpKmVdm8g5skvgc+ozKASf+92f DkZrZFIPa+UhlRidqLTQryxj4z70/qOlLN7aqioSZ4wHAMhnrr8/IiOPhNutj+sX JFwo9opPZZfncFNkTQ3tmDisYCX13WNJCEdTJY3UxYRpZfqd9NEgVrXPTEMh7nyM Llp2YniFlqUSXFejZI2+gkAqoziVisTdodAYK7DqJN6gAxo8j0wd14I7FPSEyIUP Omv+sWnXLxacfRf92maCoWYQxVsCAwEAAaNdMFswHQYDVR0OBBYEFIpYleRVSiX/ fl4IUAt7HxLlxYDOMDoGA1UdEQQzMDGCC3NoaWIuemN1LmN6hiJodHRwczovL3No aWIuemN1LmN6L2lkcC9zaGliYm9sZXRoMA0GCSqGSIb3DQEBCwUAA4IBgQBg/6xf hyyakGE4o+C6+JWISfip/uETZSIYm9HLjHphUs5N3chHfMPiTcoKMhYq5Rf+CL1p 2i3qtZXvKF576GKEEkSbPuoA5F03sxXPr1PU+F2iQGiuLPwwANUD7uXoy4j6eTvn 8MNrnX2LdUpbTQAE6zeUhJIojwWn9uxxt+85x/p+yuwc1eIq7HOAp9yzHmciA6lX E8r/XVGpclOOuXQ9HrP1AvT5bWv7LBKF7KxJAmxtoA37+13qFBC0hSTDoZiHJn1/ jXjnNIy3jttGaj9w5qJirZWA0EyJDdH8BqRLN5ZFjOys2p3aXKvIBOGK3SLLqRYf lE2e0tZFmd/BXlky4MAH9DBLFEGbn0PgBMboqh8CvoUU4I9P+slAB8YrLE/csLU8 8iGs0kFvDfGNv7GqgPJUf8s9B1JvPpD6CmW69HY1Ier3ly0LngSiPKzShPbuVlVB jyWcriD3Vh0RIEFDtfVx32wA4lgQguigbO0Vi7dhOHCqTC+earGCWvGUF+I=',
                                1 => 'MIIDFzCCAf+gAwIBAgIUCDrOzILCIUYr3TrnUQPEsHWa+wMwDQYJKoZIhvcNAQEF BQAwFjEUMBIGA1UEAxMLc2hpYi56Y3UuY3owHhcNMTIwNjE1MTIwNzE1WhcNMzIw NjE1MTIwNzE1WjAWMRQwEgYDVQQDEwtzaGliLnpjdS5jejCCASIwDQYJKoZIhvcN AQEBBQADggEPADCCAQoCggEBAIZDgPqv9Pzl5boNk7nXJpoxPt4AVs++zbuJNPU8 kJWFdeXrjB2m2wGSDm+1X33NrnkGs2LrNqSqjfXdoPM7UwBKUzqZx6qNSsDssrKt S3vK8RfyOss31NXhY6tgG4nw1YhO4324spqFIA7oXMR/QxuPXHnRxUMZjln0XgeM hAeHcJJYU/c8ZbLPr4M7eVeQSnzccvkdIc1Hil5qpsdpvb48yvUCOKl7Flm/iVLM OKlj1i4IKGSQNd8cUnZZukWK62/H1pr7Al6368zJqkYKvUJbRNfRiXEeV3c9pcg/ /bx/j66ebUulE4A4ITpE5oQvMpgx/1w2h76mY9rHzTHBMp0CAwEAAaNdMFswOgYD VR0RBDMwMYILc2hpYi56Y3UuY3qGImh0dHBzOi8vc2hpYi56Y3UuY3ovaWRwL3No aWJib2xldGgwHQYDVR0OBBYEFHLlm9ZAYoCIoT1NQsdo7sPzShFWMA0GCSqGSIb3 DQEBBQUAA4IBAQAHtEngv7KaSiRJxFaLn3VdzH6f3SC4xNGKTPyGVq0pBrwUitf4 kMl17K3Hg73ef1D+wInl3+9IuoCDsTH2wKKD3tUSZsL+xgekRGiIu3LIFcoa+U+o 5n1nnfZ7xfvh8lgmsRZ7TVC3dXFl9wU+BmsVvOWEjneOA5oZQ2MUkKDC9+5sh6jV BXl4hMGBSLfs19/xa81PR7nkC9e6N0ri6nq6rT/SwoXXsAxHMHRz9M9gSq5lwxYO d+orcDxYVF3fdNbigr2b4qcgMeVygTsdcroU8EncIRMRLw0Wdb5Pk86KmCj74dRq C9OOk807XNhUC499735YXOv2kFUlCx5mZeOk',
                            ],
                            'encryption' => [
                                0 => 'MIIEFzCCAn+gAwIBAgIUQiBRUmbK3JD/kzqcKQx8S2IiYMYwDQYJKoZIhvcNAQEL BQAwFjEUMBIGA1UEAwwLc2hpYi56Y3UuY3owHhcNMjAwNzA5MTUxODM0WhcNNDAw NzA5MTUxODM0WjAWMRQwEgYDVQQDDAtzaGliLnpjdS5jejCCAaIwDQYJKoZIhvcN AQEBBQADggGPADCCAYoCggGBAJpYZwtE1KohYxIdQr5Ah0hQF4gXtGYsTVGW6zZU dlz+bwurWNJWxx6q7HvaXELaSWPOhZpfUIQfCLizKoLk2BMSurse7rMHZ8yl5ElR Sa7uYthemNXrXe+EMdLBXyVdG5mbzfN671Ju6ma7h67Lo7WqD930TSSQnaJ6khSP H4z8IExBmlrLRdLCcpZg5abuAYfKZhnXDiCRp1424Tg0/e1ROAiUjtJ8XZNbBvEK NCCE3fnRRCmLH0laUdMupIRYT4K1AfzoZn3FFfYdeC9sqZyREvMNq8K3E7aSqvrI Zvj+dDmJYgH+UrWNEc+uqiyuI+4v36efyvyLyAwxo3VX34JuMVCfjE+Vn4j/Fp2Y CZ5a1yY3b/eQX/XCjXcRNLjSuq4XCNMbD0mt0SVCXAcbt20cvsrQbfZI99ypGTa1 h3CvZH8kFpaZRydP6axm0DtNaH9omcuUB/ZbRsUxjcVEXIZnywoVlXPajTbH5ZJh h/9Tx4PP19QKitAm108qivquDwIDAQABo10wWzAdBgNVHQ4EFgQUktg/wByRcJFK DzrP3PJOQHb/ZQQwOgYDVR0RBDMwMYILc2hpYi56Y3UuY3qGImh0dHBzOi8vc2hp Yi56Y3UuY3ovaWRwL3NoaWJib2xldGgwDQYJKoZIhvcNAQELBQADggGBAGxv1A+K 2xl2Ej11hfs9La1Y8b6yGYksf6nEPeySMr6FSGfLnNVDHkRDSeDg9Td5KaSjdIok RHnmQqOdCVgqskPZb1DZu1bhM1uxDP3qkOW1kGP6S1Inl1UCiurzmi82YZ5r78A2 auE8quI86ZhrauhpaCwAc2/etLffseB5VizJPr3RYC7/dpzlhIOpZjuLzoz4PMAM srZGyDxzQbvBxjWfGdkn8Q0RvQ+1f2lf5vyNMCg4sux1RMEXQnQpKCBTb/su4snE bWNNYDmVrr5WBcoM+8w4gSg5OU7czncF5EgD6sBrlnoDI8bU9h6Ia61JsdEQMD9P kIFKI/eSpiOnaVWLbD/JxjLHCOZvU4Fj1V0NwkJ3OpB7A024AzEaJQH4SsigdBJe gy1F4qvjGfnPqZk9aSfE5j4KP4jmBwGAKpFqrfh4rApsK6AS5dNPWDaBsbGDv+w1 ttoBlccr2E1zGuP9v2S0Ei7VFn5zuiJAc9k7KBsiDcox3jI+s6RoXJKRlQ==',
                            ],
                        ],
                    ],
                ];

                // Initializes toolkit with the array provided.
              //  $auth = new Auth($settings);
                // or
                // $settings = new \OneLogin_Saml2_Settings($settings);

                $login = $auth->login();



                // TODO auth oproti SSO


                */
    }

    /**
     * @return string[]
     */
    private function getCredentials(\stdClass $data): array
    {
        $credentials = [];
        try {
            $credentials['username'] = $this->propertyAccessor->getValue($data, $this->options['username_path']);

            if (!\is_string($credentials['username']) || '' === $credentials['username']) {
                throw new BadRequestHttpException(sprintf('The key "%s" must be a non-empty string.', $this->options['username_path']));
            }
        } catch (AccessException $e) {
            throw new BadRequestHttpException(sprintf('The key "%s" must be provided.', $this->options['username_path']), $e);
        }

        try {
            $credentials['password'] = $this->propertyAccessor->getValue($data, $this->options['password_path']);
            $this->propertyAccessor->setValue($data, $this->options['password_path'], null);

            if (!\is_string($credentials['password']) || '' === $credentials['password']) {
                throw new BadRequestHttpException(sprintf('The key "%s" must be a non-empty string.', $this->options['password_path']));
            }
        } catch (AccessException $e) {
            throw new BadRequestHttpException(sprintf('The key "%s" must be provided.', $this->options['password_path']), $e);
        }

        return $credentials;
    }
}
