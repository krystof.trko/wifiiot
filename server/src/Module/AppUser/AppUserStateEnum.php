<?php

namespace App\Module\AppUser;

enum AppUserStateEnum: string
{
    case NO_ACCESS = 'NO_ACCESS';
    case ACTIVE = 'ACTIVE';
    case INACTIVE = 'INACTIVE';
    case INACTIVE_VIOLATION = 'INACTIVE_VIOLATION';

    public function text(): string
    {
        return match ($this) {
            AppUserStateEnum::NO_ACCESS => 'Bez přístupu',
            AppUserStateEnum::ACTIVE => 'Aktivní',
            AppUserStateEnum::INACTIVE => 'Neaktivní',
            AppUserStateEnum::INACTIVE_VIOLATION => 'Neaktivní - porušení pravidel'
        };
    }

    /**
     * Cases name->enum instance.
     *
     * @return array<string, self>
     */
    public static function nameEnumList(): array
    {
        $enum = [];

        foreach (AppUserStateEnum::cases() as $case) {
            $enum[$case->name] = $case;
        }

        return $enum;
    }
}
