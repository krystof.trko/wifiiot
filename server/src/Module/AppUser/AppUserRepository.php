<?php

namespace App\Module\AppUser;

use App\Entity\AppUser;
use App\Entity\RefreshToken;
use App\Module\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;
use GraphQL\Error\UserError;

/**
 * @extends BaseRepository<AppUser>
 */
class AppUserRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppUser::class);
    }

    /**
     * @param array<string, string> $by
     *
     * @return RefreshToken[]
     */
    public function findRefreshToken(array $by): array
    {
        $em = $this->getEntityManager();

        /** @var BaseRepository<RefreshToken> $repo */
        $repo = $em->getRepository(RefreshToken::class);

        return $repo->findBy($by);
    }

    /**
     * @param array<string, string> $by
     *
     * @return ?RefreshToken
     */
    public function findOneRefreshToken(array $by): ?RefreshToken
    {
        $em = $this->getEntityManager();

        /** @var BaseRepository<RefreshToken> $repo */
        $repo = $em->getRepository(RefreshToken::class);

        $result = $repo->findOneBy($by);

        if (!$result) {
            $reflect = new \ReflectionClass(get_parent_class(self::class));
            throw new UserError('Nenalezeno '.$reflect->getShortName().': '.print_r($by, true));
        }

        return $result;
    }
}
