<?php

namespace App\Module\AppUser;

use App\Entity\AppUser;
use App\Entity\RefreshToken;
use App\Entity\UserNotification;
use App\Module\BaseResolver;
use App\Module\UserNotification\UserNotificationRepository;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<AppUser>
 */
class AppUserResolver extends BaseResolver
{
    public function __construct(private AppUserRepository $appUserRepo, private Security $security, private UserNotificationRepository $notificationRepo)
    {
        parent::__construct($appUserRepo, $security);
    }

    /**
     * Současně přihlášený uživatel.
     *
     * @param \ArrayObject<string, Request> $request
     *
     * @return array<string, AppUser|RefreshToken[]|UserNotification[]>
     */
    public function me(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): array
    {
        $user = $this->security->getUser();
        assert($user instanceof AppUser);

        $tokenList = $this->appUserRepo->findRefreshToken(['username' => $user->username]);

        // Potřeba vždy čerstvé z db
        $notificationList = $this->notificationRepo->findBy(['isShown' => false, 'forUser' => $user->id]);

        return [
            'appUser' => $user,
            'tokenList' => $tokenList,
            'notificationList' => $notificationList,
        ];
    }

    /**
     * Smazat refresh token podle ID.
     *
     * @param \ArrayObject<string, Request> $request
     *
     * @throws UserError
     */
    public function removeOneRefreshTokenBy(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): bool
    {
        $entity = $this->appUserRepo->findOneRefreshToken($argument->getArrayCopy());

        /* @phpstan-ignore-next-line nepatri do userRepo */
        return $this->appUserRepo->remove($entity);
    }

    /**
     * Nová appUser.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function create(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): AppUser
    {
        if (!$this->user->isAdmin) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        $argArr = $argument->getArrayCopy();
        $appUser = new AppUser(...$argArr['appUser']);

        return $this->appUserRepo->save($appUser);
    }

    /**
     * Upravit appUser.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): AppUser
    {
        if (!$this->user->isAdmin) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        $argArr = $argument->getArrayCopy();

        return $this->updatePropertyAll(
            $argArr['appUser'],
            ['username', 'name', 'isAdmin', 'state'],
            ['id' => $argArr['id']]
        );
    }
}
