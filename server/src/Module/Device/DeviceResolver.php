<?php

namespace App\Module\Device;

use App\Entity\Device;
use App\Entity\DevicePhoto;
use App\Entity\DevicePort;
use App\Module\AppUser\AppUserRepository;
use App\Module\BaseResolver;
use App\Module\DeviceGroup\DeviceGroupRepository;
use App\Module\DevicePhoto\DevicePhotoRepository;
use App\Module\DevicePort\DevicePortRepository;
use App\Module\StaticIp\StaticIpRepository;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Relay\Connection\Output\Connection;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<Device>
 */
class DeviceResolver extends BaseResolver
{
    public function __construct(
        private DeviceRepository $deviceRepo,
        private AppUserRepository $appUserRepo,
        private StaticIpRepository $staticIpRepo,
        private DeviceGroupRepository $deviceGroupRepo,
        private DevicePortRepository $devicePortRepo,
        private DevicePhotoRepository $devicePhotoRepo,
        Security $security
    ) {
        parent::__construct($deviceRepo, $security);
    }

    /**
     * Všechny výsledky.
     *
     * @param \ArrayObject<string, Request> $request
     *
     * @return Connection<Device>
     */
    public function allFilter(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): Connection
    {
        $by = [];

        if (DeviceFilterEnum::ALL === $argument['filter']) {
            // Může zobrazit jen admin
            if (
                !$this->user->isAdmin
            ) {
                throw new UserError('Uživatel nemá oprávnění');
            }
        }

        if (DeviceFilterEnum::MY === $argument['filter'] && isset($this->user->id)) {
            $by = ['owner' => $this->user->id];
        }

        if (DeviceFilterEnum::UNASSIGNED === $argument['filter']) {
            $by = ['owner' => null];
        }

        return $this->all($arg, $argument, $request, $resolveInfo, $by);
    }

    /**
     * New Device.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function create(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): Device
    {
        $argArr = $argument->getArrayCopy();

        // Může přidat zařízení jen sobě
        if (
            !$this->user->isAdmin
            && $this->user->id != $argArr['device']['ownerId']
        ) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        // Najít entitu majitele
        $argArr['device']['owner'] = null;
        if ($argArr['device']['ownerId']) {
            $argArr['device']['owner'] = $this->appUserRepo->findOneByErr(['id' => $argArr['device']['ownerId'] ?? '']);
        }
        unset($argArr['device']['ownerId']);

        // Najít entitu staticIp, neni povinny
        if (!empty($argArr['device']['staticIpId'])) {
            $argArr['device']['staticIp'] = $this->staticIpRepo->findOneByErr(['id' => $argArr['device']['staticIpId']]);

            if ($argArr['device']['staticIp']->device) {
                throw new UserError('Statická IP adresa je obsazena');
            }
        } else {
            $argArr['device']['staticIp'] = null;
        }
        unset($argArr['device']['staticIpId']);

        // Najít existující skupiny a přidat do entity
        $groupList = $this->deviceGroupRepo->findByIdErr($argArr['device']['deviceGroupIdList'] ?? []);
        unset($argArr['device']['deviceGroupIdList']);

        // Uložit port list
        $devicePortList = $argArr['device']['devicePortList'] ?? [];
        unset($argArr['device']['devicePortList']);

        // Uložit photo list
        $devicePhotoList = $argArr['device']['devicePhotoList'] ?? [];
        unset($argArr['device']['devicePhotoList']);

        $device = new Device(...$argArr['device']);

        // Nastaveni správného device, pokud je obsazeno
        if (!empty($argArr['device']['staticIp'])) {
            $argArr['device']['staticIp']->device = $device;
        }

        // Přidat kategorie
        foreach ($groupList as $groupEntity) {
            $device->deviceGroupList->add($groupEntity);
        }

        // Vytvořit nové přesměrování portů
        $this->devicePortRepo->checkDuplicite('hubPort', array_column($devicePortList, 'hubPort'));

        foreach ($devicePortList as $devicePortData) {
            // unset($devicePortData['deviceId']);
            $devicePortEntity = new DevicePort($device, ...$devicePortData);
            $device->devicePortList->add($devicePortEntity);
        }

        // Přidat nové fotky
        $this->devicePhotoRepo->checkDuplicite('filename', array_column($devicePhotoList, 'filename'));

        foreach ($devicePhotoList as $devicePhotoData) {
            // unset($devicePhotoData['photoId']);
            $devicePhotoEntity = new DevicePhoto($device, ...$devicePhotoData);
            $device->devicePhotoList->add($devicePhotoEntity);
        }

        try {
            $this->deviceRepo->save($device);
        } catch (\Exception $e) {
            throw new UserError($e);
        }

        return $device;
    }

    /**
     * Upravit Device.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): Device
    {
        $argArr = $argument->getArrayCopy();

        // Najít entitu majitele
        if (!empty($argArr['device']['ownerId'])) {
            // Může přidat zařízení jen sobě
            if (
                !$this->user->isAdmin
                && $this->user->id != $argArr['device']['ownerId']
            ) {
                throw new UserError('Uživatel nemá oprávnění');
            }

            $argArr['device']['owner'] = $this->appUserRepo->findOneByErr(['id' => $argArr['device']['ownerId']]);
            unset($argArr['device']['ownerId']);
        }

        $newStaticIp = null;

        // Najít entitu staticIp
        if (!empty($argArr['device']['staticIpId'])) {
            $newStaticIp = $this->staticIpRepo->findOneByErr(['id' => $argArr['device']['staticIpId']]);

            if ($newStaticIp->device) {
                throw new UserError('Statická IP adresa je obsazena');
            }

            unset($argArr['device']['staticIpId']);
        }

        $device = $this->updatePropertyAll(
            $argArr['device'],
            ['name', 'mac', 'ip', 'isKnown', 'isKnownExpiration', 'owner', 'hasInternetAccess', 'internetAccessExpiration', 'wifiName', 'wifiPass', 'isAllowed'],
            ['id' => $argArr['id']]
        );

        // Nastaveni správného device, pokud je obsazeno
        if ($newStaticIp) {
            // Pokud mělo ip, je potřeba smazat
            if ($device->staticIp) {
                $device->staticIp->device = null;
                $this->staticIpRepo->save($device->staticIp);
                $device->staticIp = null;
                $this->deviceRepo->save($device);
            }

            $newStaticIp->device = $device;
            $this->staticIpRepo->save($newStaticIp);
        }

        return $device;
    }

    /**
     * Přidat zařízení do skupiny.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function addToGroup(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): bool
    {
        $argArr = $argument->getArrayCopy();

        $device = $this->deviceRepo->findOneByErr(['id' => $argArr['deviceId'] ?? '']);
        $group = $this->deviceGroupRepo->findOneByErr(['id' => $argArr['groupId'] ?? '']);

        // Může přidat jen svoje zařízení do svojí skupiny
        if (
            !$this->user->isAdmin
            && ($this->user->id != $device->owner?->id
                || $this->user->id != $group->owner?->id)
        ) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        if (!$device->deviceGroupList->contains($group)) {
            $device->deviceGroupList->add($group);
        }

        $this->deviceRepo->save($device);

        return true;
    }

    /**
     * Odebrat zařízení ze skupiny.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function removeFromGroup(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): bool
    {
        $argArr = $argument->getArrayCopy();

        $device = $this->deviceRepo->findOneByErr(['id' => $argArr['deviceId'] ?? '']);
        $group = $this->deviceGroupRepo->findOneByErr(['id' => $argArr['groupId'] ?? '']);

        // Může smazat jen svoje zařízení ze svojí skupiny
        if (
            !$this->user->isAdmin
            && ($this->user->id != $device->owner?->id
                || $this->user->id != $group->owner?->id)
        ) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        if ($device->deviceGroupList->contains($group)) {
            $device->deviceGroupList->removeElement($group);
        }

        $this->deviceRepo->save($device);

        return true;
    }
}
