<?php

namespace App\Module\Device;

enum DeviceFilterEnum: string
{
    case MY = 'MY';
    case UNASSIGNED = 'UNASSIGNED';
    case ALL = 'ALL';

    public function text(): string
    {
        return match ($this) {
            DeviceFilterEnum::MY => 'Moje',
            DeviceFilterEnum::UNASSIGNED => 'Nepřiřazená',
            DeviceFilterEnum::ALL => 'Všechna',
        };
    }

    /**
     * Cases name->enum instance.
     *
     * @return array<string, self>
     */
    public static function nameEnumList(): array
    {
        $enum = [];

        foreach (DeviceFilterEnum::cases() as $case) {
            $enum[$case->name] = $case;
        }

        return $enum;
    }
}
