<?php

namespace App\Module\DevicePhoto;

use App\Entity\DevicePhoto;
use App\Module\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends BaseRepository<DevicePhoto>
 */
class DevicePhotoRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DevicePhoto::class);
    }
}
