<?php

namespace App\Module\DevicePhoto;

use App\Entity\DevicePhoto;
use App\Module\BaseResolver;
use App\Module\Device\DeviceRepository;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<DevicePhoto>
 */
class DevicePhotoResolver extends BaseResolver
{
    public function __construct(
        private DevicePhotoRepository $devicePhotoRepo,
        private DeviceRepository $deviceRepo,
        private DevicePhotoUploader $uploader,
        Security $security
    ) {
        parent::__construct($devicePhotoRepo, $security);
    }

    /**
     * New DevicePhoto.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function create(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): DevicePhoto
    {
        $argArr = $argument->getArrayCopy();

        // Najít entitu zařízení
        $argArr['devicePhoto']['device'] = $this->deviceRepo->findOneByErr(['id' => $argArr['devicePhoto']['deviceId'] ?? '']);
        unset($argArr['devicePhoto']['deviceId']);

        $argArr['devicePhoto']['filename'] = $this->uploader->upload($argArr['devicePhoto']['file']);
        unset($argArr['devicePhoto']['file']);

        // Může přidat jen ke svému zařízení
        if (
            !$this->user->isAdmin
            && $this->user->id != $argArr['devicePhoto']['device']->owner->id
        ) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        $appUser = new DevicePhoto(...$argArr['devicePhoto']);

        return $this->devicePhotoRepo->save($appUser);
    }

    /**
     * Upravit DevicePhoto.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): DevicePhoto
    {
        $argArr = $argument->getArrayCopy();

        return $this->updatePropertyAll(
            $argArr['devicePhoto'],
            ['displayOrder'],
            ['id' => $argArr['id']]
        );
    }

    /**
     * Smazat foto z db i adr.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function removePhoto(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): bool
    {
        $entity = $this->repo->findOneByErr($argument->getArrayCopy());

        $this->uploader->remove($entity->filename);

        return $this->repo->remove($entity);
    }
}
