<?php

namespace App\Module\DevicePhoto;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DevicePhotoUploader
{
    public function __construct(private string $uploadDir)
    {
    }

    public function upload(UploadedFile $file): string
    {
        $fileName = sha1($file->getContent()).uniqid().'.'.$file->guessExtension();
        $file->move($this->uploadDir, $fileName);

        return $fileName;
    }

    /**
     * Smazat.
     */
    public function remove(string $fileName): bool
    {
        $path = $this->uploadDir.$fileName;

        $fs = new Filesystem();
        $fs->remove($path);

        return true;
    }
}
