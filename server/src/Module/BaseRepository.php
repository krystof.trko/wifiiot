<?php

namespace App\Module;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Overblog\GraphQLBundle\Error\UserError;

/**
 * @template T of object
 *
 * @template-extends ServiceEntityRepository<T>
 */
abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * Uložit do db nyní a navrátit výsledek.
     *
     * @param T $obj
     *
     * @return T
     */
    public function save(object $obj): object
    {
        $em = $this->getEntityManager();

        $em->persist($obj);

        try {
            $em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new UserError('Duplicitní hodnota');
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new UserError('Chyba cizího klíče');
        }

        return $obj;
    }

    /**
     * Smazat z db nyní a navrátit true.
     *
     * @param T $obj
     */
    public function remove(object $obj): bool
    {
        $em = $this->getEntityManager();

        $em->remove($obj);

        try {
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new UserError('Chyba cizího klíče');
        }

        return true;
    }

    /**
     * Jeden záznam s ošetřením chyby.
     *
     * @param array<string, string> $by
     *
     * @return T
     *
     * @throws UserError
     */
    public function findOneByErr(array $by): object
    {
        $result = $this->findOneBy($by);

        if (!$result) {
            $reflect = new \ReflectionClass(get_parent_class(self::class));
            throw new UserError('Nenalezeno '.$reflect->getShortName().': '.print_r($by, true));
        }

        return $result;
    }

    /**
     * Všechny záznamy s ID a ošetřením chyby.
     *
     * @param string[] $requestedIds
     *
     * @return T[]
     *
     * @throws UserError
     */
    public function findByIdErr(array $requestedIds): array
    {
        $list = $this->findBy(['id' => $requestedIds]);

        $foundIds = array_map(function ($d) {
            assert(isset($d->id));

            return $d->id;
        }, $list);

        $idDiff = array_diff($requestedIds, $foundIds);

        if ($idDiff) {
            $reflect = new \ReflectionClass(get_parent_class(self::class));
            throw new UserError('Nenalezené '.$reflect->getShortName().' s id(s) '.implode(', ', $idDiff));
        }

        return $list;
    }

    /**
     * Kontrola jestli není přesměrováno více portů na jednom hubu.
     *
     * @param string[] $items
     *
     * @throws UserError
     */
    public function checkDuplicite(string $propetry, array $items): void
    {
        $list = $this->findBy([$propetry => $items]);
        $foundItems = array_map(fn ($d) => $d->{$propetry}, $list);

        if (count($foundItems) > 0) {
            throw new UserError('Duplicita '.$propetry.' '.implode(', ', $foundItems));
        }
    }
}
