<?php

namespace App\Module\StaticIp;

use App\Entity\StaticIp;
use App\Module\BaseResolver;
use App\Module\Device\DeviceRepository;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Error\UserError;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<StaticIp>
 */
class StaticIpResolver extends BaseResolver
{
    public function __construct(private StaticIpRepository $staticIpRepo, private DeviceRepository $deviceRepo, Security $security)
    {
        parent::__construct($staticIpRepo, $security);
    }

    /**
     * New StaticIp.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function create(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): StaticIp
    {
        if (!$this->user->isAdmin) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        $argArr = $argument->getArrayCopy();

        if ($this->staticIpRepo->count(['ip' => $argArr['staticIp']['ip']]) > 0) {
            throw new UserError('Statická adresa '.$argArr['staticIp']['ip'].' je použita');
        }

        // Najít entitu zařízení
        if (!empty($argArr['staticIp']['deviceId'])) {
            $argArr['staticIp']['device'] = $this->deviceRepo->findOneByErr(['id' => $argArr['staticIp']['deviceId'] ?? '']);

            if ($this->staticIpRepo->count(['device' => $argArr['staticIp']['device']]) > 0) {
                throw new UserError('Zařízení '.$argArr['staticIp']['device']->id.' už má statickou adresu');
            }
        } else {
            $argArr['staticIp']['device'] = null;
        }

        unset($argArr['staticIp']['deviceId']);

        $appUser = new StaticIp(...$argArr['staticIp']);

        return $this->staticIpRepo->save($appUser);
    }

    /**
     * Upravit StaticIp.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): StaticIp
    {
        if (!$this->user->isAdmin) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        $argArr = $argument->getArrayCopy();

        // Najít entitu zařízení
        if (!empty($argArr['staticIp']['deviceId'])) {
            $argArr['staticIp']['device'] = $this->deviceRepo->findOneByErr(['id' => $argArr['staticIp']['deviceId']]);
            unset($argArr['staticIp']['deviceId']);
        }

        return $this->updatePropertyAll(
            $argArr['staticIp'],
            ['device', 'ip', 'expiration'],
            ['id' => $argArr['id']]
        );
    }
}
