<?php

namespace App\Module\StaticIp;

use App\Entity\StaticIp;
use App\Module\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends BaseRepository<StaticIp>
 */
class StaticIpRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StaticIp::class);
    }
}
