<?php

namespace App\Module\DeviceGroup;

enum DeviceGroupFilterEnum: string
{
    case MY = 'MY';
    case ALL = 'INACTIVE';

    public function text(): string
    {
        return match ($this) {
            DeviceGroupFilterEnum::MY => 'Moje',
            DeviceGroupFilterEnum::ALL => 'Všechna',
        };
    }

    /**
     * Cases name->enum instance.
     *
     * @return array<string, self>
     */
    public static function nameEnumList(): array
    {
        $enum = [];

        foreach (DeviceGroupFilterEnum::cases() as $case) {
            $enum[$case->name] = $case;
        }

        return $enum;
    }
}
