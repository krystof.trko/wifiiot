<?php

namespace App\Module\DeviceGroup;

use App\Entity\DeviceGroup;
use App\Module\AppUser\AppUserRepository;
use App\Module\BaseResolver;
use App\Module\Device\DeviceRepository;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Relay\Connection\Output\Connection;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<DeviceGroup>
 */
class DeviceGroupResolver extends BaseResolver
{
    public function __construct(
        private DeviceGroupRepository $deviceGroupRepo,
        private AppUserRepository $appUserRepo,
        private DeviceRepository $deviceRepo,
        Security $security
    ) {
        parent::__construct($deviceGroupRepo, $security);
    }

    /**
     * Všechny výsledky.
     *
     * @param \ArrayObject<string, Request> $request
     *
     * @return Connection<DeviceGroup>
     */
    public function allFilter(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): Connection
    {
        $by = [];

        if (DeviceGroupFilterEnum::ALL === $argument['filter']) {
            // Může zobrazit jen admin
            if (
                !$this->user->isAdmin
            ) {
                throw new UserError('Uživatel nemá oprávnění');
            }
        }

        if (DeviceGroupFilterEnum::MY === $argument['filter'] && isset($this->user->id)) {
            $by = ['owner' => $this->user->id];
        }

        return $this->all($arg, $argument, $request, $resolveInfo, $by);
    }

    /**
     * New DeviceGroup.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function create(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): DeviceGroup
    {
        $argArr = $argument->getArrayCopy();

        // Může přidat skupinu jen sobě
        if (
            !$this->user->isAdmin
            && $this->user->id != $argArr['deviceGroup']['ownerId']
        ) {
            throw new UserError('Uživatel nemá oprávnění');
        }

        // Najít entitu majitele
        $argArr['deviceGroup']['owner'] = null;
        if ($argArr['deviceGroup']['ownerId']) {
            $argArr['deviceGroup']['owner'] = $this->appUserRepo->findOneByErr(['id' => $argArr['deviceGroup']['ownerId'] ?? '']);
        }
        unset($argArr['deviceGroup']['ownerId']);

        // Najít existující zařízení a přidat do entity
        $deviceList = $this->deviceRepo->findByIdErr($argArr['deviceGroup']['deviceIdList'] ?? []);
        unset($argArr['deviceGroup']['deviceIdList']);

        $deviceGroup = new DeviceGroup(...$argArr['deviceGroup']);

        // Přidat kategorie
        foreach ($deviceList as $deviceEntity) {
            $deviceEntity->deviceGroupList->add($deviceGroup);
        }

        return $this->deviceGroupRepo->save($deviceGroup);
    }

    /**
     * Upravit DeviceGroup.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): DeviceGroup
    {
        $argArr = $argument->getArrayCopy();

        // Najít entitu majitele
        if (!empty($argArr['deviceGroup']['ownerId'])) {
            // Může upravit skupinu jen sobě
            if (
                !$this->user->isAdmin
                && $this->user->id != $argArr['deviceGroup']['ownerId']
            ) {
                throw new UserError('Uživatel nemá oprávnění');
            }

            $argArr['deviceGroup']['owner'] = $this->appUserRepo->findOneByErr(['id' => $argArr['deviceGroup']['ownerId']]);
            unset($argArr['deviceGroup']['ownerId']);
        }

        return $this->updatePropertyAll(
            $argArr['deviceGroup'],
            ['name', 'owner', 'isActive'],
            ['id' => $argArr['id']]
        );
    }
}
