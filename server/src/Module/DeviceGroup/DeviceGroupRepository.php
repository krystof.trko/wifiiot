<?php

namespace App\Module\DeviceGroup;

use App\Entity\DeviceGroup;
use App\Module\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends BaseRepository<DeviceGroup>
 */
class DeviceGroupRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeviceGroup::class);
    }
}
