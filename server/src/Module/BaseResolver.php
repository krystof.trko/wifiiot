<?php

namespace App\Module;

use App\Entity\AppUser;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Relay\Connection\Output\Connection;
use Overblog\GraphQLBundle\Relay\Connection\Paginator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @template T of object
 */
abstract class BaseResolver
{
    protected AppUser $user;

    /**
     * Nastavení repozitáře.
     *
     * @param BaseRepository<T> $repo
     */
    public function __construct(protected BaseRepository $repo, Security $security)
    {
        $user = $security->getUser();
        assert($user instanceof AppUser);

        $this->user = $user;
    }

    /**
     * Všechny výsledky.
     *
     * @param \ArrayObject<string, Request>  $request
     * @param array<string, string|int|null> $by
     *
     * @return Connection<T>
     */
    public function all(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo, array $by = []): Connection
    {
        $paginator = new Paginator(
            function (?int $offset, ?int $limit) use ($by) {
                return $this->repo->findBy($by, [], $limit, $offset);
            }
        );

        $result = $paginator->auto($argument, fn () => $this->repo->count());

        assert($result instanceof Connection);

        return $result;
    }

    /**
     * Podle ID.
     *
     * @param \ArrayObject<string, Request> $request
     *
     * @return T|null
     *
     * @throws UserError
     */
    public function findOneBy(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): ?object
    {
        return $this->repo->findOneByErr($argument->getArrayCopy());
    }

    /**
     * Smazat podle ID.
     *
     * @param \ArrayObject<string, Request> $request
     *
     * @throws UserError
     */
    public function removeOneBy(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): bool
    {
        $entity = $this->repo->findOneByErr($argument->getArrayCopy());

        return $this->repo->remove($entity);
    }

    /**
     * Updatovat daný objekt.
     *
     * @param array<string, string> $propertyVal
     * @param string[]              $updatable
     * @param array<string, string> $findByCriteria
     *
     * @return T
     *
     * @throws UserError
     */
    protected function updatePropertyAll(array $propertyVal, array $updatable, array $findByCriteria): object
    {
        $entity = $this->repo->findOneByErr($findByCriteria);

        foreach ($propertyVal as $property => $value) {
            if (!in_array($property, $updatable)) {
                throw new UserError('Vlastnost '.$property.' nelze měnit');
            }

            $entity->{$property} = $value;
        }

        $this->repo->save($entity);

        return $entity;
    }
}
