<?php

namespace App\Module\UserNotification;

use App\Entity\UserNotification;
use App\Module\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends BaseRepository<UserNotification>
 */
class UserNotificationRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserNotification::class);
    }
}
