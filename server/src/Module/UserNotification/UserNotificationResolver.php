<?php

namespace App\Module\UserNotification;

use App\Entity\UserNotification;
use App\Module\BaseResolver;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<UserNotification>
 */
class UserNotificationResolver extends BaseResolver
{
    public function __construct(Security $security, UserNotificationRepository $notificationRepo)
    {
        parent::__construct($notificationRepo, $security);
    }

    /**
     * Upravit userNotifcation.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): UserNotification
    {
        $argArr = $argument->getArrayCopy();

        return $this->updatePropertyAll(
            $argArr['userNotification'],
            ['isShown'],
            ['id' => $argArr['id']]
        );
    }
}
