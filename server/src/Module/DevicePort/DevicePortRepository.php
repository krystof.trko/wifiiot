<?php

namespace App\Module\DevicePort;

use App\Entity\DevicePort;
use App\Module\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends BaseRepository<DevicePort>
 */
class DevicePortRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DevicePort::class);
    }
}
