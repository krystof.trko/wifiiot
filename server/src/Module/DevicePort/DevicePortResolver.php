<?php

namespace App\Module\DevicePort;

use App\Entity\DevicePort;
use App\Module\BaseResolver;
use App\Module\Device\DeviceRepository;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @extends BaseResolver<DevicePort>
 */
class DevicePortResolver extends BaseResolver
{
    public function __construct(
        private DevicePortRepository $devicePortRepo,
        private DeviceRepository $deviceRepo,
        Security $security
    ) {
        parent::__construct($devicePortRepo, $security);
    }

    /**
     * New DevicePort.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function create(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): DevicePort
    {
        $argArr = $argument->getArrayCopy();

        $this->devicePortRepo->checkDuplicite('hubPort', [$argArr['devicePort']['hubPort']]);

        // Najít entitu zařízení
        $argArr['devicePort']['device'] = $this->deviceRepo->findOneByErr(['id' => $argArr['devicePort']['deviceId'] ?? '']);
        unset($argArr['devicePort']['deviceId']);

        $appUser = new DevicePort(...$argArr['devicePort']);

        return $this->devicePortRepo->save($appUser);
    }

    /**
     * Upravit DevicePort.
     *
     * @param \ArrayObject<string, Request> $request
     */
    public function update(mixed $arg, Argument $argument, \ArrayObject $request, ResolveInfo $resolveInfo): DevicePort
    {
        $argArr = $argument->getArrayCopy();

        // Kontrola duplicity portu
        if (!empty($argArr['devicePort']['hubPort'])) {
            $this->devicePortRepo->checkDuplicite('hubPort', [$argArr['devicePort']['hubPort']]);
        }

        // Najít entitu zařízení
        if (!empty($argArr['devicePort']['deviceId'])) {
            // Může přidat jen ke svému zařízení
            if (
                !$this->user->isAdmin
                && $this->user->id != $argArr['devicePort']['deviceId']
            ) {
                throw new UserError('Uživatel nemá oprávnění');
            }

            $argArr['devicePort']['device'] = $this->deviceRepo->findOneByErr(['id' => $argArr['devicePort']['deviceId']]);
            unset($argArr['devicePort']['deviceId']);
        }

        return $this->updatePropertyAll(
            $argArr['devicePort'],
            ['device', 'hubPort', 'devicePort', 'expiration'],
            ['id' => $argArr['id']]
        );
    }
}
