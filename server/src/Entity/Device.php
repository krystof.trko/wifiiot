<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\Device\DeviceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Zařízení přihlášené do sítě, nebo příprava pro přihlášení.
 */
#[ORM\Entity(repositoryClass: DeviceRepository::class), ORM\HasLifecycleCallbacks]
class Device
{
    /**
     * ID zařízení v db.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    /**
     * Obsaženo ve skupinách.
     *
     * @var Collection<int, DeviceGroup>
     */
    #[ManyToMany(targetEntity: DeviceGroup::class, inversedBy: 'deviceList', cascade: ['persist', 'remove'])]
    public Collection $deviceGroupList;

    /**
     * Přesměrování portů na IP hubu.
     *
     * @var Collection<int, DevicePort>
     */
    #[OneToMany(targetEntity: DevicePort::class, mappedBy: 'device', orphanRemoval: true, cascade: ['persist', 'remove'])]
    public Collection $devicePortList;

    /**
     * Fotky zařízení.
     *
     * @var Collection<int, DevicePhoto>
     */
    #[OneToMany(targetEntity: DevicePhoto::class, mappedBy: 'device', orphanRemoval: true, cascade: ['persist', 'remove'])]
    public Collection $devicePhotoList;

    public function __construct(
        /**
         * Přiřazené jméno.
         */
        #[ORM\Column(length: 255)]
        public string $name,

        /**
         * MAC zařízení připojeného do sítě např. E8:DB:84:AB:74:3E.
         */
        #[ORM\Column(length: 17)]
        public string $mac,

        /**
         * IP zařízení připojeného do sítě např. 192.168.1.1.
         */
        #[ORM\Column(length: 15, nullable: true)]
        public ?string $ip,

        /**
         * Zařízení je známo a přiřazeno uživateli.
         */
        #[ORM\Column]
        public bool $isKnown,

        /**
         * Expriace isKnown - nutné prodloužit, nebo znovu přiřadit.
         */
        #[ORM\Column(nullable: true)]
        public ?\DateTime $isKnownExpiration,

        /**
         * Zařízení přiřazeno uživateli.
         */
        #[ManyToOne(targetEntity: AppUser::class, inversedBy: 'deviceList')]
        #[JoinColumn(onDelete: 'SET NULL')]
        public ?AppUser $owner,

        /**
         * Povolený přístup do internetu. Platné jen pokud je isKnown a isAllowed.
         */
        #[ORM\Column]
        public bool $hasInternetAccess,

        /**
         * Datum platnosti přítupu do internetu.
         */
        #[ORM\Column(nullable: true)]
        public ?\DateTime $internetAccessExpiration,

        /**
         * Přiřazená statická adresa ze seznamu dostupných IP.
         */
        #[OneToOne(targetEntity: StaticIp::class, mappedBy: 'device', cascade: ['persist'])]
        public ?StaticIp $staticIp,

        /**
         * Přístupové údaje do wifi - jméno.
         */
        #[ORM\Column(length: 255, nullable: true)]
        public ?string $wifiName,

        /**
         * Přístupové údaje do wifi - heslo.
         */
        #[ORM\Column(length: 255, nullable: true)]
        public ?string $wifiPass,

        /**
         * Zařízení je povoleno. Může být zakázáno např. v případě incidentu.
         */
        #[ORM\Column]
        public bool $isAllowed,
    ) {
        $this->deviceGroupList = new ArrayCollection();
        $this->devicePortList = new ArrayCollection();
        $this->devicePhotoList = new ArrayCollection();
    }
}
