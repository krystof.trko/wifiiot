<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\DeviceGroup\DeviceGroupRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Notifikace pro uživatele.
 */
#[ORM\Entity(repositoryClass: DeviceGroupRepository::class), ORM\HasLifecycleCallbacks]
class UserNotification
{
    /**
     * Id notifikace v databázi.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    public function __construct(
        /**
         * Zpráva notifikace.
         */
        #[ORM\Column(length: 255)]
        public string $text,

        /**
         * Notifikace pro uživatele.
         */
        #[ManyToOne(targetEntity: AppUser::class, inversedBy: 'deviceGroupList')]
        #[JoinColumn(onDelete: 'SET NULL')]
        public ?AppUser $forUser,

        /**
         * Notifikace byla zobrazena.
         */
        #[ORM\Column]
        public bool $isShown = false,
    ) {
    }
}
