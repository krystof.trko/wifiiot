<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\DevicePort\DevicePortRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Přesměrování portů k zařízení na hub.
 */
#[ORM\Entity(repositoryClass: DevicePortRepository::class), ORM\HasLifecycleCallbacks, UniqueConstraint('hubPort', ['hub_port'])]
class DevicePort
{
    /**
     * Id přesměrování v databázi.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    public function __construct(
        /**
         * Zařízení, které má tento port přesměrovaný.
         */
        #[ManyToOne(targetEntity: Device::class, inversedBy: 'devicePortList', cascade: ['persist'])]
        public Device $device,

        /**
         * Veřejný port na hubu. Musí být unikátní.
         */
        #[ORM\Column]
        public int $hubPort,

        /**
         * Port na zařízení.
         */
        #[ORM\Column]
        public int $devicePort,

        /**
         * Expirace přiřazení.
         */
        #[ORM\Column(nullable: true)]
        public ?\DateTime $expiration = null,
    ) {
    }
}
