<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\AppUser\AppUserRepository;
use App\Module\AppUser\AppUserStateEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Uživatel autorizovaný přes jednotné přihlašování.
 */
#[ORM\Entity(repositoryClass: AppUserRepository::class), ORM\HasLifecycleCallbacks]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_USERNAME', fields: ['username'])]
class AppUser implements UserInterface
{
    /**
     * ID zařízení v db.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    /**
     * Zařízení přidělená uživateli.
     *
     * @var Collection<int, Device>
     */
    #[OneToMany(targetEntity: Device::class, mappedBy: 'owner')]
    public Collection $deviceList;

    /**
     * Skupiny vytvořené uživatelem.
     *
     * @var Collection<int, DeviceGroup>
     */
    #[OneToMany(targetEntity: DeviceGroup::class, mappedBy: 'owner')]
    public Collection $deviceGroupList;

    /**
     * Refresh tokeny pro obnovu JWT.
     *
     * @var Collection<int, RefreshToken>
     */
    #[OneToMany(targetEntity: RefreshToken::class, mappedBy: 'owner')]
    public Collection $refreshTokenList;

    /**
     * Notifikace pro uživatele.
     *
     * @var Collection<int, UserNotification>
     */
    #[OneToMany(targetEntity: UserNotification::class, mappedBy: 'forUser')]
    public Collection $notificationList;

    /**
     * @var list<string> Role uživatele
     */
    #[ORM\Column]
    private array $roles = [];

    /**
     * Poslední přihlášení uživatele.
     */
    #[ORM\Column(nullable: true)]
    public ?\DateTime $lastLoginAt;

    public function __construct(
        /**
         * Uživatelské jméno.
         */
        #[ORM\Column(length: 255)]
        public string $username,

        /**
         * Celé jméno uživatele.
         */
        #[ORM\Column(length: 255)]
        public string $name,

        /**
         * Administrátorská oprávnění.
         */
        #[ORM\Column]
        public bool $isAdmin,

        /**
         * Uživatel je povolen/zakázán.
         */
        #[ORM\Column(type: 'string', enumType: AppUserStateEnum::class)]
        public AppUserStateEnum $state,
    ) {
        $this->deviceList = new ArrayCollection();
        $this->lastLoginAt = null;

        $this->deviceList = new ArrayCollection();
        $this->deviceGroupList = new ArrayCollection();
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
