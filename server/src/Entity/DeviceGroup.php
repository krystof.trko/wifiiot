<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\DeviceGroup\DeviceGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Skupina zařízení, která mezi sebou mohou komunikovat.
 */
#[ORM\Entity(repositoryClass: DeviceGroupRepository::class), ORM\HasLifecycleCallbacks]
class DeviceGroup
{
    /**
     * Id skupiny v databázi.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    /**
     * Seznam zařízení ve skupině.
     *
     * @var Collection<int, Device>
     */
    #[ManyToMany(targetEntity: Device::class, mappedBy: 'deviceGroupList', cascade: ['persist', 'remove'])]
    public Collection $deviceList;

    public function __construct(
        /**
         * Jméno skupiny.
         */
        #[ORM\Column(length: 255)]
        public string $name,

        /**
         * Skupinu vytvořil a spravuje.
         */
        #[ManyToOne(targetEntity: AppUser::class, inversedBy: 'deviceGroupList')]
        #[JoinColumn(onDelete: 'SET NULL')]
        public ?AppUser $owner,

        /**
         * Skupina je aktivní a zařízení mohou komunikovat.
         */
        #[ORM\Column]
        public bool $isActive,
    ) {
        $this->deviceList = new ArrayCollection();
    }
}
