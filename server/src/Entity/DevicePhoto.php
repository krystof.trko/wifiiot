<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\DevicePort\DevicePortRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Fotky zařízení.
 */
#[ORM\Entity(repositoryClass: DevicePortRepository::class), ORM\HasLifecycleCallbacks, UniqueConstraint('filename', ['filename'])]
class DevicePhoto
{
    /**
     * Id souboru v databázi.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    public function __construct(
        /**
         * Zařízení s touto fotkou.
         */
        #[ManyToOne(targetEntity: Device::class, inversedBy: 'devicePhotoList', cascade: ['persist'])]
        public Device $device,

        /**
         * Cesta k souboru.
         */
        #[ORM\Column]
        public string $filename,

        /**
         * Pořadí fotky.
         */
        #[ORM\Column]
        public int $displayOrder,
    ) {
    }
}
