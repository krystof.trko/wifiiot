<?php

namespace App\Entity;

use App\EntityAttributes\Id;
use App\EntityAttributes\Timestampable;
use App\Module\StaticIp\StaticIpRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Dostupné statické adresy k přiřazení.
 */
#[ORM\Entity(repositoryClass: StaticIpRepository::class), ORM\HasLifecycleCallbacks]
class StaticIp
{
    /**
     * Id přesměrování v databázi.
     */
    use Id;

    /**
     * Datum vytvoření záznamu
     * Datum poslední úpravy záznamu.
     */
    use Timestampable;

    public function __construct(
        /**
         * Zařízení, které má tuto ip přidělenou.
         */
        #[OneToOne(targetEntity: Device::class, inversedBy: 'staticIp', cascade: ['persist'])]
        #[JoinColumn(onDelete: 'SET NULL')]
        public ?Device $device,

        /**
         * Datum expriace přidělení této vip k zařízení.
         */
        #[ORM\Column(nullable: true)]
        public ?\DateTime $expiration,

        /**
         * Veřejná IP.
         */
        #[ORM\Column(length: 15)]
        public ?string $ip,
    ) {
    }
}
