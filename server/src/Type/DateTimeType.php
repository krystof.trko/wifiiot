<?php

namespace App\Type;

use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;

class DateTimeType extends ScalarType
{
    private const string DATE_FORMAT = \DateTime::RFC3339_EXTENDED;

    /**
     * Serializovat.
     */
    public function serialize(mixed $value): string
    {
        if (!($value instanceof \DateTime)) {
            throw new Error('Not DateTime: '.print_r($value, true));
        }

        return $value->format(self::DATE_FORMAT);
    }

    /**
     * Parsovat.
     */
    public function parseValue(mixed $value): \DateTime
    {
        if (!is_string($value)) {
            throw new Error('Must be string: '.print_r($value, true));
        }

        $dateTime = \DateTime::createFromFormat(self::DATE_FORMAT, $value);

        if (!$dateTime) {
            $dateTime = \DateTime::createFromFormat(\DateTime::RFC3339, $value);
        }

        if (!$dateTime) {
            $dateTime = \DateTime::createFromFormat(\DateTime::ISO8601, $value);
        }

        if (!$dateTime) {
            $dateTime = \DateTime::createFromFormat(\DateTime::ISO8601_EXPANDED, $value);
        }

        if (!$dateTime) {
            $dateTime = \DateTime::createFromFormat('Y-m-d\\TH:i:s', $value);
        }

        if (!$dateTime) {
            throw new Error('Cannot parse: '.$value);
        }

        return $dateTime;
    }

    /**
     * Parse literal.
     *
     * @param mixed[]|null $variables
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null): \DateTime
    {
        if (empty($valueNode->value)) {
            throw new Error('Not DateTime $valueNode->value not exist: '.print_r($valueNode, true));
        }

        $dateTime = \DateTime::createFromFormat($valueNode->value, self::DATE_FORMAT);

        if (!$dateTime) {
            throw new Error('Cannot parse: '.print_r($valueNode->value, true));
        }

        return $dateTime;
    }
}
