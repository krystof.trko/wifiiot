<?php

namespace App\Type;

use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;

class CursorType extends ScalarType
{
    /**
     * Serializovat.
     */
    public function serialize(mixed $value): string
    {
        return $this->validate($value);
    }

    /**
     * Parsovat.
     */
    public function parseValue(mixed $value): string
    {
        return $this->validate($value);
    }

    /**
     * Parse literal.
     *
     * @param mixed[]|null $variables
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null): string
    {
        if (empty($valueNode->value)) {
            throw new Error('Not Cursor $valueNode->value not exist: '.print_r($valueNode, true));
        }

        return $this->validate($valueNode->value);
    }

    /**
     * Is input valid.
     */
    private function validate(mixed $in): string
    {
        if (!is_string($in)) {
            throw new Error('Not Cursor: '.print_r($in, true));
        }

        return (string) $in;
    }
}
