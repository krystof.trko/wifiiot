<?php

namespace App\Type;

use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;

class PortType extends ScalarType
{
    /**
     * Serializovat.
     */
    public function serialize(mixed $value): int
    {
        return $this->validate($value);
    }

    /**
     * Parsovat.
     */
    public function parseValue(mixed $value): int
    {
        return $this->validate($value);
    }

    /**
     * Parse literal.
     *
     * @param mixed[]|null $variables
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null): int
    {
        if (empty($valueNode->value)) {
            throw new Error('Not PORT $valueNode->value not exist: '.print_r($valueNode, true));
        }

        return $this->validate($valueNode->value);
    }

    /**
     * Is input valid.
     */
    private function validate(mixed $in): int
    {
        if (!is_scalar($in)) {
            throw new Error('Not scalar port value: '.print_r($in, true));
        }

        $val = (int) $in;

        if ($val < 1 || $val > 65535) {
            throw new Error("Invalid port value: {$val}");
        }

        return $val;
    }
}
