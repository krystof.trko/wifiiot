<?php

namespace App\Type;

use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;

class MacType extends ScalarType
{
    /**
     * Serializovat.
     */
    public function serialize(mixed $value): string
    {
        return $this->validate($value);
    }

    /**
     * Parsovat.
     */
    public function parseValue(mixed $value): string
    {
        return $this->validate($value);
    }

    /**
     * Parse literal.
     *
     * @param mixed[]|null $variables
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null): string
    {
        if (empty($valueNode->value)) {
            throw new Error('Not MAC $valueNode->value not exist: '.print_r($valueNode, true));
        }

        return $this->validate($valueNode->value);
    }

    /**
     * Is input valid.
     */
    private function validate(mixed $in): string
    {
        if (!is_scalar($in) || !filter_var($in, FILTER_VALIDATE_MAC)) {
            throw new Error('Not MAC: '.print_r($in, true));
        }

        if (5 !== substr_count((string) $in, ':')) {
            throw new Error('Invalid MAC notation: '.print_r($in, true));
        }

        return (string) $in;
    }
}
