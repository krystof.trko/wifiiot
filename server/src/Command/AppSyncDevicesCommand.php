<?php

namespace App\Command;

use App\Entity\Device;
use App\Entity\UserNotification;
use App\Module\AppUser\AppUserRepository;
use App\Module\Device\DeviceRepository;
use App\Module\UserNotification\UserNotificationRepository;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'App:SyncDevices',
    description: 'Synchronizovat zařízení',
)]
class AppSyncDevicesCommand extends Command
{
    private Generator $faker;

    public function __construct(private DeviceRepository $deviceRepo, private AppUserRepository $userRepo, private UserNotificationRepository $notificationRepo)
    {
        $this->faker = Factory::create();
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $ip = $this->faker->optional()->ipv4();

        $device = new Device(
            '',
            $this->faker->macAddress(),
            $ip,
            false,
            null,
            null,
            false,
            null,
            null,
            null,
            null,
            false
        );

        $this->deviceRepo->save($device);

        $adminAll = $this->userRepo->findBy(['isAdmin' => true]);

        foreach ($adminAll as $admin) {
            $userNotification = new UserNotification('Nové zařízení s IP: '.$ip, $admin);
            $this->notificationRepo->save($userNotification);
        }

        $io->success('Zařízení synchronizována');

        return Command::SUCCESS;
    }
}
