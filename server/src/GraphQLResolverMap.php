<?php

namespace App;

use App\Module\AppUser\AppUserResolver;
use App\Module\AppUser\AppUserStateEnum;
use App\Module\Device\DeviceFilterEnum;
use App\Module\Device\DeviceResolver;
use App\Module\DeviceGroup\DeviceGroupFilterEnum;
use App\Module\DeviceGroup\DeviceGroupResolver;
use App\Module\DevicePhoto\DevicePhotoResolver;
use App\Module\DevicePort\DevicePortResolver;
use App\Module\StaticIp\StaticIpResolver;
use App\Module\UserNotification\UserNotificationResolver;
use App\Type\CursorType;
use App\Type\DateTimeType;
use App\Type\IPv4Type;
use App\Type\MacType;
use App\Type\PortType;
use Overblog\GraphQLBundle\Resolver\ResolverMap;
use Overblog\GraphQLBundle\Upload\Type\GraphQLUploadType;

class GraphQLResolverMap extends ResolverMap
{
    public function __construct(
        private AppUserResolver $appUser,
        private DeviceResolver $device,
        private DeviceGroupResolver $deviceGroup,
        private DevicePhotoResolver $devicePhoto,
        private DevicePortResolver $devicePort,
        private StaticIpResolver $staticIp,
        private UserNotificationResolver $userNotification,
    ) {
    }

    protected function map()
    {
        return [
            'Query' => [
                'me' => [$this->appUser, 'me'],
                'appUser' => [$this->appUser, 'findOneBy'],
                'appUserList' => [$this->appUser, 'all'],

                'device' => [$this->device, 'findOneBy'],
                'deviceList' => [$this->device, 'allFilter'],

                'deviceGroup' => [$this->deviceGroup, 'findOneBy'],
                'deviceGroupList' => [$this->deviceGroup, 'allFilter'],

                'devicePhoto' => [$this->devicePhoto, 'findOneBy'],
                'devicePhotoList' => [$this->devicePhoto, 'all'],

                'devicePort' => [$this->devicePort, 'findOneBy'],
                'devicePortList' => [$this->devicePort, 'all'],

                'staticIp' => [$this->staticIp, 'findOneBy'],
                'staticIpList' => [$this->staticIp, 'all'],
            ],
            'Mutation' => [
                'createAppUser' => [$this->appUser, 'create'],
                'createDevice' => [$this->device, 'create'],
                'createDeviceGroup' => [$this->deviceGroup, 'create'],
                'createDevicePhoto' => [$this->devicePhoto, 'create'],
                'createDevicePort' => [$this->devicePort, 'create'],
                'createStaticIp' => [$this->staticIp, 'create'],

                'updateAppUser' => [$this->appUser, 'update'],
                'updateDevice' => [$this->device, 'update'],
                'updateDeviceGroup' => [$this->deviceGroup, 'update'],
                'updateDevicePhoto' => [$this->devicePhoto, 'update'],
                'updateDevicePort' => [$this->devicePort, 'update'],
                'updateStaticIp' => [$this->staticIp, 'update'],
                'updateUserNotification' => [$this->userNotification, 'update'],

                'removeAppUser' => [$this->appUser, 'removeOneBy'],
                'removeDevice' => [$this->device, 'removeOneBy'],
                'removeDeviceGroup' => [$this->deviceGroup, 'removeOneBy'],
                'removeDevicePhoto' => [$this->devicePhoto, 'removePhoto'],
                'removeDevicePort' => [$this->devicePort, 'removeOneBy'],
                'removeStaticIp' => [$this->staticIp, 'removeOneBy'],
                'removeRefreshToken' => [$this->appUser, 'removeOneRefreshTokenBy'],

                'addDeviceToGroup' => [$this->device, 'addToGroup'],
                'removeDeviceFromGroup' => [$this->device, 'removeFromGroup'],
            ],
            'DateTime' => [self::SCALAR_TYPE => new DateTimeType()],
            'Port' => [self::SCALAR_TYPE => new PortType()],
            'MAC' => [self::SCALAR_TYPE => new MacType()],
            'IPv4' => [self::SCALAR_TYPE => new IPv4Type()],
            'Cursor' => [self::SCALAR_TYPE => new CursorType()],
            'PhotoUpload' => [self::SCALAR_TYPE => new GraphQLUploadType()],
            'APP_USER_STATE' => AppUserStateEnum::nameEnumList(),
            'DEVICE_FILTER' => DeviceFilterEnum::nameEnumList(),
            'DEVICE_GROUP_FILTER' => DeviceGroupFilterEnum::nameEnumList(),
        ];
    }
}
