const uri = 'http://localhost:8102/'
const prefix = ''

export default {
    uri: {
        api: uri + prefix,
        login: uri + prefix + 'login',
        refresh: uri + prefix + 'refresh',
        devicePhoto: uri + 'device_photo/',
    },
}
