import { App_User_State } from "@/gql/graphql"

const stateTranslationTable = {
    [App_User_State.Active]: 'Aktivní',
    [App_User_State.Inactive]: 'Neaktivní',
    [App_User_State.InactiveViolation]: 'Nekativní - pravidla',
    [App_User_State.NoAccess]: 'Bez přístupu',
}

const tableFlipped = Object.fromEntries(Object.entries(stateTranslationTable).map(a => a.reverse()))

const stateList = Object.values(stateTranslationTable)

const traslateState = (state: App_User_State): string => stateTranslationTable[state]
const untraslateState = (state: App_User_State): string => tableFlipped[state]

export {stateList, stateTranslationTable, traslateState, untraslateState}