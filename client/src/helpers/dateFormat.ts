export function ymdhm(date: Date | undefined | null | string): string {
    if (date === null || date === undefined || date === '') {
        return '-'
    }

    let dateObj = date

    if (typeof date === 'string') {
        dateObj = new Date(date)
    }

    // Pokud není datum, vráti string jak je
    if (dateObj.toString() === 'Invalid Date' || typeof dateObj === 'string') {
        return ''
    }

    return new Intl.DateTimeFormat('cs-CZ', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
    }).format(dateObj)
}
