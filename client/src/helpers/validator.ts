import {
    helpers,
    required,
    between,
    macAddress,
    ipAddress,
    minLength,
    maxLength,
} from '@vuelidate/validators'

export const required$ = helpers.withMessage('musí být vyplněno', required)

export const macAddress$ = helpers.withMessage(
    'musí být v formátu E8:DB:84:AB:74:3E',
    macAddress(':')
)

export const ipAddress$ = helpers.withMessage(
    'musí být v formátu 192.168.1.1',
    ipAddress
)

export const between$ = (min: number, max: number) =>
    helpers.withMessage(
        ({ $params }) => `musí být mezi ${$params.min} a ${$params.max}.`,
        between(min, max)
    )

export const minLength$ = (min: number) =>
    helpers.withMessage(
        ({ $params }) => `musí obsahovat alespoň ${$params.min} znaky.`,
        minLength(min)
    )

export const maxLength$ = (max: number) =>
    helpers.withMessage(
        ({ $params }) => `musí obsahovat nejvýše ${$params.max} znaků.`,
        maxLength(max)
    )
