import { toastController } from '@ionic/vue'
import { close, information, thumbsDown, thumbsUp } from 'ionicons/icons'

export default (
    header: string,
    color: string,
    icon: string = ''
): Promise<void> => {
    if (icon === '') {
        if (color === 'danger') {
            icon = thumbsDown
        }
        if (color === 'warning') {
            icon = information
        }
        if (color === 'success') {
            icon = thumbsUp
        }
    }

    return toastController
        .create({
            // positive
            header: header,
            position: 'top',
            duration: 8000,
            color: color,
            buttons: [
                {
                    icon: close,
                    htmlAttributes: {
                        'aria-label': 'close',
                    },
                },
            ],
            icon: icon,
        })
        .then((toast) => toast.present())
}
