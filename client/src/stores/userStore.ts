import { userAuthService, apolloClient } from '@/boot/api'
import { defineStore } from 'pinia'
import { provideApolloClient, useQuery } from '@vue/apollo-composable'
import gql from 'graphql-tag'
import { App_User_State, AppUser } from '@/gql/graphql'

export const useUserStore = defineStore('user', {
    state: () => ({
        data: <AppUser>{
            id: '',
            username: '',
            name: '',
            isAdmin: false,
            state: App_User_State.NoAccess,
            deviceGroupList: [],
            deviceList: [],
            roles: [],
            lastLoginAt: '',
            createdAt: '',
            updatedAt: '',
        },
        logged: false,
    }),
    getters: {},
    actions: {
        async loadData(): Promise<void> {
            const query = provideApolloClient(apolloClient)(() =>
                useQuery(
                    gql`
                        query Me {
                            me {
                                appUser {
                                    id
                                    username
                                    name
                                    isAdmin
                                    state
                                    roles
                                    lastLoginAt
                                    createdAt
                                    updatedAt
                                }
                            }
                        }
                    `,
                    null,
                    { fetchPolicy: 'network-only' }
                )
            )

            return new Promise((resolve, reject) => {
                query.onResult((result) => {
                    if (result?.data?.me?.appUser) {
                        this.data = result.data.me.appUser
                        resolve()
                    } else {
                        reject()
                    }
                })
            })
        },

        /**
         * Spouští se před každou kontrolou routy
         */
        async init(): Promise<void> {
            // Nekontrolovat pokud je přihášen
            if (this.logged) {
                return
            }

            const result = await userAuthService.hasToken()

            if (result) {
                this.logged = true
                await this.loadData()
            }
        },

        async logIn(credentials: {
            username: string
            password: string
        }): Promise<void> {
            await userAuthService.logIn(credentials)
            this.logged = true
            await this.loadData()
        },

        async logOut(): Promise<void> {
            await userAuthService.logOut()
            this.$reset()
        },
    },
})
