import { Route } from "@/router/route"
import { defineStore } from "pinia"
import { RouteRecordName } from "vue-router"

export const useGlobalStore = defineStore('global', {
    state: () => (<{
        darkMode: boolean,
        selectedMenu: RouteRecordName
    }>{
        darkMode: false,
        selectedMenu: Route.home
    }),
    getters: {},
    actions: {
        /**
         * Načíst preferenci a hlídat její změnu
         */
        init(): void{
            // Preference uživatele
            const prefersDark = window.matchMedia('(prefers-color-scheme: dark)')
            this.darkModeOn(prefersDark.matches)

            // Čekat na změnu
            prefersDark.addEventListener('change', (mediaQuery) =>
                this.darkModeOn(mediaQuery.matches)
            )
        },

        /**
         * Zapnutí/vypnutí tmavého režimu přidáním/odebráním třídy ion-palette-dark
         */
        darkModeOn(isActive: boolean): void {
            document.documentElement.classList.toggle('ion-palette-dark', isActive)
            this.darkMode = isActive
        },
    },
})
