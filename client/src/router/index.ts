import { createRouter, createWebHistory } from '@ionic/vue-router'
import { Route } from '@/router/route'
import { useUserStore } from '@/stores/userStore'
import routeList from './routeList'
import toast from '@/helpers/toast'
import { useGlobalStore } from '@/stores/globalStore'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routeList,
})

// Auth
router.beforeEach((to, from, next) => {
    const userStore = useUserStore()
    const globalStore = useGlobalStore()

    if (to.name) {
        globalStore.selectedMenu = to.name
    }

    if (to.name === Route.notFound) {
        next()
        return
    }

    // Uživatel není přihlášen
    if (to.name !== Route.login && !userStore.logged) {
        toast('Prosím přihlašte se', 'warning')
        router.push({ name: Route.login })
    }

    // Uživatel je již přihlášen
    if (to.name === Route.login && userStore.logged) {
        toast('Již jste přihlášen', 'success')
        router.push({ name: Route.home })
    }

    next()
})

export default router
