export enum Route {
    // Index
    home = 'home',
    // Device
    deviceList = 'deviceList',
    device = 'device',
    // Group
    groupList = 'groupList',
    group = 'group',

    // AppUser
    userList = 'userList',
    login = 'login',
    me = 'me',

    // StaticIp
    staticIpList = 'staticIpList',

    // 404
    notFound = 'notFound',
}
