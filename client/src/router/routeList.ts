import { RouteRecordRaw } from 'vue-router'
import { Route } from '@/router/route'

const routeList: Array<RouteRecordRaw> = [
    /* Index */
    {
        path: '/',
        name: Route.home,
        component: () => import('@/views/UserHomepage.vue'),
    },

    /* Devices */
    {
        path: '/zarizeni-seznam',
        name: Route.deviceList,
        component: () => import('@/views/Device/DeviceList.vue'),
    },
    {
        path: '/zarizeni/:id?',
        name: Route.device,
        component: () => import('@/views/Device/DeviceOne.vue'),
    },

    /* DeviceGroup */
    {
        path: '/skupiny',
        name: Route.groupList,
        component: () => import('@/views/DeviceGroup/GroupList.vue'),
    },
    {
        path: '/skupina/:id?',
        name: Route.group,
        component: () => import('@/views/DeviceGroup/GroupOne.vue'),
    },

    /* AppUser */
    {
        path: '/uzivatele',
        name: Route.userList,
        component: () => import('@/views/AppUser/UserList.vue'),
    },
    {
        path: '/prihlasit',
        name: Route.login,
        component: () => import('@/views/AppUser/UserLogIn.vue'),
    },
    {
        path: '/ja',
        name: Route.me,
        component: () => import('@/views/AppUser/UserMe.vue'),
    },

    /* StaticIp */
    {
        path: '/verejne-adresy',
        name: Route.staticIpList,
        component: () => import('@/views/StaticIp/StaticIpList.vue'),
    },

    /* 404 */
    {
        path: '/:catchAll(.*)*',
        name: Route.notFound,
        component: () => import('@/views/ErrorNotFound.vue'),
    },
]

export default routeList
