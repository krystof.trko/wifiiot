import SimplexNoise from './simplexNoise'

export default () => {
    type layer = {
        id: number
        progress: number
        color: string
    }

    class App {
        private simplex: any
        private config
        private canvas
        private ctx
        private shadowCanvas
        private shadowCtx
        private timestamp = 0
        private wHeight = 0
        private wWidth = 0
        private wCenterX = 0
        private wCenterY = 0
        private wHypot = 0
        private angle = 0
        private layers: layer[] = []

        constructor() {
            this.config = {
                bgColor: '#E0E3F4',
                colorSchema: [
                    '#8BBF9F',
                    '#A7ACD9',
                    '#D7816A',
                    '#f6f8fc',
                    '#2f2f2f',
                ],
                numOfLayers: 9,
            }

            this.simplex = new SimplexNoise()

            this.canvas = <HTMLCanvasElement>(
                document.getElementById('canvas404')
            )

            if (!this.canvas) {
                return
            }

            this.ctx = this.canvas.getContext('2d')

            this.shadowCanvas = document.createElement('canvas')

            if (!this.shadowCanvas) {
                return
            }

            this.shadowCtx = this.shadowCanvas.getContext('2d')

            this.timestamp = 0

            this.setUpVars()
            // listeners
            window.addEventListener('resize', this.setUpVars.bind(this))
            this.update()
        }

        getLayers(): layer[] {
            const layers = []
            let currColorId = 0

            for (let lid = 0; lid <= this.config.numOfLayers; lid++) {
                layers.push({
                    id: lid, // used for noise offset
                    progress: 1 - lid / this.config.numOfLayers,
                    color: this.config.colorSchema[currColorId],
                })

                currColorId++

                if (currColorId >= this.config.colorSchema.length) {
                    currColorId = 0
                }
            }

            return layers
        }

        setUpVars() {
            if (!this.shadowCanvas) {
                return
            }

            this.canvas.width =
                this.shadowCanvas.width =
                this.wWidth =
                    window.innerWidth
            this.canvas.height =
                this.shadowCanvas.height =
                this.wHeight =
                    window.innerHeight
            this.wCenterX = this.wWidth / 2
            this.wCenterY = this.wHeight / 2
            this.wHypot = Math.hypot(this.wWidth, this.wHeight)

            this.angle = Math.PI * 0.25
            this.layers = this.getLayers()
        }

        drawLayer(ctx: CanvasRenderingContext2D, layer: layer) {
            const segmentBaseSize = 10
            const segmentCount = Math.round(this.wHypot / segmentBaseSize)
            const segmentSize = this.wHypot / segmentCount
            const waveAmplitude = segmentSize * 4
            const noiseZoom = 0.03

            ctx.save()
            ctx.translate(this.wCenterX, this.wCenterY)
            ctx.rotate(Math.sin(this.angle))

            ctx.beginPath()
            ctx.moveTo(
                -this.wHypot / 2,
                this.wHypot / 2 - this.wHypot * layer.progress
            )
            ctx.lineTo(-this.wHypot / 2, this.wHypot / 2)
            ctx.lineTo(this.wHypot / 2, this.wHypot / 2)
            ctx.lineTo(
                this.wHypot / 2,
                this.wHypot / 2 - this.wHypot * layer.progress
            )

            for (let sid = 1; sid <= segmentCount; sid++) {
                const n = this.simplex.noise3D(
                    sid * noiseZoom,
                    sid * noiseZoom,
                    layer.id + this.timestamp
                )
                const heightOffset = n * waveAmplitude

                ctx.lineTo(
                    this.wHypot / 2 - sid * segmentSize,
                    this.wHypot / 2 -
                        this.wHypot * layer.progress +
                        heightOffset
                )
            }

            ctx.closePath()
            ctx.fillStyle = layer.color
            ctx.fill()
            ctx.restore()
        }

        draw(ctx: CanvasRenderingContext2D) {
            ctx.save()
            ctx.fillStyle = this.config.bgColor
            ctx.fillRect(0, 0, this.wWidth, this.wHeight)
            ctx.restore()

            this.layers.forEach((layer) => this.drawLayer(ctx, layer))
        }

        update(t: number = 0) {
            if (!this.shadowCtx || !this.ctx || !this.shadowCanvas) {
                return
            }

            if (t) {
                let shiftNeeded = false
                this.timestamp = t / 5000
                this.angle += 0.001

                this.layers.forEach((layer) => {
                    layer.progress += 0.001

                    if (layer.progress > 1 + 1 / (this.layers.length - 1)) {
                        layer.progress = 0
                        shiftNeeded = true
                    }
                })

                if (shiftNeeded) {
                    const layer = this.layers.shift()
                    if (layer) {
                        this.layers.push()
                    }
                }

                this.draw(this.shadowCtx)
            }

            this.draw(this.shadowCtx)

            this.ctx.clearRect(0, 0, this.wWidth, this.wHeight)
            this.ctx.drawImage(this.shadowCanvas, 0, 0)

            window.requestAnimationFrame(this.update.bind(this))
        }
    }

    new App()
}
