/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** Kurzor pro paginator */
  Cursor: { input: any; output: any; }
  /** Datum a čas ve formátu ISO8601 */
  DateTime: { input: any; output: any; }
  /** Notace . např. 192.168.1.1 */
  IPv4: { input: any; output: any; }
  /** Notace : např. E8:DB:84:AB:74:3E */
  MAC: { input: any; output: any; }
  /** Pro nahrání fotky */
  PhotoUpload: { input: any; output: any; }
  /** Port 1 - 65535 */
  Port: { input: any; output: any; }
};

export enum App_User_State {
  /** Aktivní */
  Active = 'ACTIVE',
  /** Neaktivní */
  Inactive = 'INACTIVE',
  /** Neaktivní z důvodu porušení pravidel */
  InactiveViolation = 'INACTIVE_VIOLATION',
  /** Nemá možný přístup - nesplňuje pravidla */
  NoAccess = 'NO_ACCESS'
}

/** Uživatel autorizovaný přes jednotné přihlašování */
export type AppUser = Timestampable & {
  __typename?: 'AppUser';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Skupiny vytvořené uživatelem */
  deviceGroupList: Array<DeviceGroup>;
  /** Zařízení přidělená uživateli */
  deviceList: Array<Device>;
  /** ID uživatele v db */
  id: Scalars['ID']['output'];
  /** Administrátorská oprávnění */
  isAdmin: Scalars['Boolean']['output'];
  /** Poslední přihlášení uživatele */
  lastLoginAt?: Maybe<Scalars['DateTime']['output']>;
  /** Celé jméno uživatele */
  name: Scalars['String']['output'];
  /** Role ve kterých je uživatel */
  roles: Array<Scalars['String']['output']>;
  /** Uživatel je povolen/zakázán */
  state: App_User_State;
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  /** Uživatelské jméno */
  username: Scalars['String']['output'];
};

export type AppUserConnection = {
  __typename?: 'AppUserConnection';
  edges: Array<AppUserEdge>;
  pageInfo: PageInfo;
};

export type AppUserEdge = {
  __typename?: 'AppUserEdge';
  cursor: Scalars['Cursor']['output'];
  node: AppUser;
};

export type AppUserWithTokens = {
  __typename?: 'AppUserWithTokens';
  /** Data uživatele */
  appUser: AppUser;
  /** Nepřečtené notifikace pro uživatele */
  notificationList: Array<UserNotification>;
  /** Tokeny uživatele */
  tokenList: Array<RefreshToken>;
};

export enum Device_Filter {
  /** Všechna */
  All = 'ALL',
  /** Moje */
  My = 'MY',
  /** Nepřiřazená */
  Unassigned = 'UNASSIGNED'
}

export enum Device_Group_Filter {
  /** Všechna */
  All = 'ALL',
  /** Moje */
  My = 'MY'
}

/** Zařízení přihlášené do sítě, nebo příprava pro přihlášení */
export type Device = Timestampable & {
  __typename?: 'Device';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Osaženo ve skupinách */
  deviceGroupList: Array<Maybe<DeviceGroup>>;
  /** Fotky zařízení */
  devicePhotoList: Array<Maybe<DevicePhoto>>;
  /** Přesměrování portů na IP hubu */
  devicePortList: Array<Maybe<DevicePort>>;
  /** Povolený přístup do internetu. Platné jen pokud je isKnown a isAllowed. */
  hasInternetAccess: Scalars['Boolean']['output'];
  /** ID zařízení v db */
  id: Scalars['ID']['output'];
  /** Datum platnosti přítupu do internetu */
  internetAccessExpiration?: Maybe<Scalars['DateTime']['output']>;
  /** IP zařízení připojeného do sítě např. 192.168.1.1 */
  ip?: Maybe<Scalars['IPv4']['output']>;
  /** Zařízení je povoleno. Může být zakázáno např. v případě incidentu */
  isAllowed: Scalars['Boolean']['output'];
  /** Zařízení je známo a přiřazeno uživateli */
  isKnown: Scalars['Boolean']['output'];
  /** Expriace isKnown - nutné prodloužit, nebo znovu přiřadit. */
  isKnownExpiration?: Maybe<Scalars['DateTime']['output']>;
  /** MAC zařízení připojeného do sítě např. E8:DB:84:AB:74:3E */
  mac: Scalars['MAC']['output'];
  /** Přiřazené jméno */
  name: Scalars['String']['output'];
  /** Zařízení přiřazeno uživateli */
  owner?: Maybe<AppUser>;
  /** Přiřazená statická adresa ze seznamu dostupných IP */
  staticIp?: Maybe<StaticIp>;
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  /** Přístupové údaje do wifi - jméno */
  wifiName?: Maybe<Scalars['String']['output']>;
  /** Přístupové údaje do wifi - heslo */
  wifiPass?: Maybe<Scalars['String']['output']>;
};

export type DeviceConnection = {
  __typename?: 'DeviceConnection';
  edges: Array<DeviceEdge>;
  pageInfo: PageInfo;
};

export type DeviceEdge = {
  __typename?: 'DeviceEdge';
  cursor: Scalars['Cursor']['output'];
  node: Device;
};

/** Skupina zařízení, která mezi sebou mohou komunikovat */
export type DeviceGroup = Timestampable & {
  __typename?: 'DeviceGroup';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Seznam zařízení ve skupině */
  deviceList: Array<Device>;
  /** Id skupiny v databázi */
  id: Scalars['ID']['output'];
  /** Skupina je aktivní a zařízení mohou komunikovat */
  isActive: Scalars['Boolean']['output'];
  /** Jméno skupiny */
  name: Scalars['String']['output'];
  /** Skupinu vytvořil a spravuje */
  owner?: Maybe<AppUser>;
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type DeviceGroupConnection = {
  __typename?: 'DeviceGroupConnection';
  edges: Array<DeviceGroupEdge>;
  pageInfo: PageInfo;
};

export type DeviceGroupEdge = {
  __typename?: 'DeviceGroupEdge';
  cursor: Scalars['Cursor']['output'];
  node: DeviceGroup;
};

/** Fotky zařízení. */
export type DevicePhoto = Timestampable & {
  __typename?: 'DevicePhoto';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Zařízení s touto fotkou */
  device: Device;
  /** Pořadí fotky */
  displayOrder: Scalars['Int']['output'];
  /** Cesta k souboru ke stažení. */
  filename: Scalars['String']['output'];
  /** Id souboru v databázi */
  id: Scalars['ID']['output'];
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type DevicePhotoConnection = {
  __typename?: 'DevicePhotoConnection';
  edges: Array<DevicePhotoEdge>;
  pageInfo: PageInfo;
};

export type DevicePhotoEdge = {
  __typename?: 'DevicePhotoEdge';
  cursor: Scalars['Cursor']['output'];
  node: DevicePhoto;
};

/** Přesměrování portů k zařízení na hub */
export type DevicePort = Timestampable & {
  __typename?: 'DevicePort';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Zařízení, které má tento port přesměrovaný */
  device: Device;
  /** Port na zařízení */
  devicePort: Scalars['Port']['output'];
  /** Expirace přiřazení */
  expiration?: Maybe<Scalars['DateTime']['output']>;
  /** Veřejný port na hubu. Musí být unikátní */
  hubPort: Scalars['Port']['output'];
  /** Id přesměrování v databázi */
  id: Scalars['ID']['output'];
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type DevicePortConnection = {
  __typename?: 'DevicePortConnection';
  edges: Array<DevicePortEdge>;
  pageInfo: PageInfo;
};

export type DevicePortEdge = {
  __typename?: 'DevicePortEdge';
  cursor: Scalars['Cursor']['output'];
  node: DevicePort;
};

/** WiFi IoT vytvoření nových a úpravy */
export type Mutation = {
  __typename?: 'Mutation';
  addDeviceToGroup: Scalars['Boolean']['output'];
  createAppUser: AppUser;
  createDevice: Device;
  createDeviceGroup: DeviceGroup;
  createDevicePhoto: DevicePhoto;
  createDevicePort: DevicePort;
  createStaticIp: StaticIp;
  removeAppUser: Scalars['Boolean']['output'];
  removeDevice: Scalars['Boolean']['output'];
  removeDeviceFromGroup: Scalars['Boolean']['output'];
  removeDeviceGroup: Scalars['Boolean']['output'];
  removeDevicePhoto: Scalars['Boolean']['output'];
  removeDevicePort: Scalars['Boolean']['output'];
  removeRefreshToken: Scalars['Boolean']['output'];
  removeStaticIp: Scalars['Boolean']['output'];
  updateAppUser: AppUser;
  updateDevice: Device;
  updateDeviceGroup: DeviceGroup;
  updateDevicePhoto: DevicePhoto;
  updateDevicePort: DevicePort;
  updateStaticIp: StaticIp;
  updateUserNotification: UserNotification;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationAddDeviceToGroupArgs = {
  deviceId: Scalars['ID']['input'];
  groupId: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationCreateAppUserArgs = {
  appUser: CreateAppUser;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationCreateDeviceArgs = {
  device: CreateDevice;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationCreateDeviceGroupArgs = {
  deviceGroup?: InputMaybe<CreateDeviceGroup>;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationCreateDevicePhotoArgs = {
  devicePhoto?: InputMaybe<CreateDevicePhoto>;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationCreateDevicePortArgs = {
  devicePort?: InputMaybe<CreateDevicePort>;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationCreateStaticIpArgs = {
  staticIp: CreateStaticIp;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveAppUserArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveDeviceArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveDeviceFromGroupArgs = {
  deviceId: Scalars['ID']['input'];
  groupId: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveDeviceGroupArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveDevicePhotoArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveDevicePortArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveRefreshTokenArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationRemoveStaticIpArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateAppUserArgs = {
  appUser: UpdateAppUser;
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateDeviceArgs = {
  device: UpdateDevice;
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateDeviceGroupArgs = {
  deviceGroup?: InputMaybe<UpdateDeviceGroup>;
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateDevicePhotoArgs = {
  devicePhoto?: InputMaybe<UpdateDevicePhoto>;
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateDevicePortArgs = {
  devicePort?: InputMaybe<UpdateDevicePort>;
  id: Scalars['ID']['input'];
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateStaticIpArgs = {
  id: Scalars['ID']['input'];
  staticIp: UpdateStaticIp;
};


/** WiFi IoT vytvoření nových a úpravy */
export type MutationUpdateUserNotificationArgs = {
  id: Scalars['ID']['input'];
  userNotification: UpdateUserNotification;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor: Scalars['String']['output'];
  hasNextPage: Scalars['Boolean']['output'];
  hasPreviousPage: Scalars['Boolean']['output'];
  startCursor: Scalars['String']['output'];
};

/** WiFi IoT dotazy */
export type Query = {
  __typename?: 'Query';
  /** Uživatel */
  appUser: AppUser;
  /** Seznam uživatelů */
  appUserList: AppUserConnection;
  /** Zařízení */
  device: Device;
  /** Skupiny zařízení */
  deviceGroup: DeviceGroup;
  /** Seznam skupin zařízení */
  deviceGroupList: DeviceGroupConnection;
  /** Seznam zařízení */
  deviceList: DeviceConnection;
  /** Fotky zařízení */
  devicePhoto: DevicePhoto;
  /** Seznam fotek zařízení */
  devicePhotoList: DevicePhotoConnection;
  /** Přesměrování zařízení */
  devicePort: DevicePort;
  /** Seznam přesměrování zařízení */
  devicePortList: DevicePortConnection;
  /** Součaně přihlášený uživatel */
  me: AppUserWithTokens;
  /** Dostupná statická adresa */
  staticIp: StaticIp;
  /** Seznam dostupných statických adres */
  staticIpList: StaticIpConnection;
};


/** WiFi IoT dotazy */
export type QueryAppUserArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT dotazy */
export type QueryAppUserListArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  first: Scalars['Int']['input'];
};


/** WiFi IoT dotazy */
export type QueryDeviceArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT dotazy */
export type QueryDeviceGroupArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT dotazy */
export type QueryDeviceGroupListArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  filter: Device_Group_Filter;
  first: Scalars['Int']['input'];
};


/** WiFi IoT dotazy */
export type QueryDeviceListArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  filter: Device_Filter;
  first: Scalars['Int']['input'];
};


/** WiFi IoT dotazy */
export type QueryDevicePhotoArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT dotazy */
export type QueryDevicePhotoListArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  first: Scalars['Int']['input'];
};


/** WiFi IoT dotazy */
export type QueryDevicePortArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT dotazy */
export type QueryDevicePortListArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  first: Scalars['Int']['input'];
};


/** WiFi IoT dotazy */
export type QueryStaticIpArgs = {
  id: Scalars['ID']['input'];
};


/** WiFi IoT dotazy */
export type QueryStaticIpListArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  first: Scalars['Int']['input'];
};

export type RefreshToken = {
  __typename?: 'RefreshToken';
  id: Scalars['ID']['output'];
  valid: Scalars['DateTime']['output'];
};

/** Dostupné statické adresy k přiřazení */
export type StaticIp = Timestampable & {
  __typename?: 'StaticIp';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Zařízení, které má tuto ip přidělenou */
  device?: Maybe<Device>;
  /** Datum expriace přidělení této vip k zařízení */
  expiration?: Maybe<Scalars['DateTime']['output']>;
  /** ID statické IP v db */
  id: Scalars['ID']['output'];
  /** Veřejná IP */
  ip: Scalars['IPv4']['output'];
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type StaticIpConnection = {
  __typename?: 'StaticIpConnection';
  edges: Array<StaticIpEdge>;
  pageInfo: PageInfo;
};

export type StaticIpEdge = {
  __typename?: 'StaticIpEdge';
  cursor: Scalars['Cursor']['output'];
  node: StaticIp;
};

export type Timestampable = {
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** ID v db */
  id: Scalars['ID']['output'];
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

/** Notifikace pro uživatele */
export type UserNotification = Timestampable & {
  __typename?: 'UserNotification';
  /** Datum vytvoření záznamu */
  createdAt: Scalars['DateTime']['output'];
  /** Notifikace pro uživatel */
  forUser?: Maybe<AppUser>;
  /** Id notifikace v databázi. */
  id: Scalars['ID']['output'];
  /** Notifikace byla zobrazena */
  isShown: Scalars['Boolean']['output'];
  /** Zpráva notifikace */
  text: Scalars['String']['output'];
  /** Datum poslední úpravy záznamu */
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type CreateAppUser = {
  isAdmin: Scalars['Boolean']['input'];
  name: Scalars['String']['input'];
  state: App_User_State;
  username: Scalars['String']['input'];
};

export type CreateDevice = {
  deviceGroupIdList: Array<Scalars['ID']['input']>;
  devicePhotoList: Array<CreateDevicePhotoFromDevice>;
  devicePortList: Array<CreateDevicePortFromDevice>;
  hasInternetAccess: Scalars['Boolean']['input'];
  internetAccessExpiration?: InputMaybe<Scalars['DateTime']['input']>;
  ip?: InputMaybe<Scalars['IPv4']['input']>;
  isAllowed: Scalars['Boolean']['input'];
  isKnown: Scalars['Boolean']['input'];
  isKnownExpiration?: InputMaybe<Scalars['DateTime']['input']>;
  mac: Scalars['MAC']['input'];
  name: Scalars['String']['input'];
  ownerId?: InputMaybe<Scalars['ID']['input']>;
  staticIpId?: InputMaybe<Scalars['ID']['input']>;
  wifiName?: InputMaybe<Scalars['String']['input']>;
  wifiPass?: InputMaybe<Scalars['String']['input']>;
};

export type CreateDeviceGroup = {
  deviceIdList: Array<Scalars['ID']['input']>;
  isActive: Scalars['Boolean']['input'];
  name: Scalars['String']['input'];
  ownerId?: InputMaybe<Scalars['ID']['input']>;
};

export type CreateDevicePhoto = {
  deviceId: Scalars['ID']['input'];
  displayOrder: Scalars['Int']['input'];
  file: Scalars['PhotoUpload']['input'];
};

export type CreateDevicePhotoFromDevice = {
  displayOrder: Scalars['Int']['input'];
  file: Scalars['PhotoUpload']['input'];
};

export type CreateDevicePort = {
  deviceId: Scalars['ID']['input'];
  devicePort: Scalars['Port']['input'];
  expiration?: InputMaybe<Scalars['DateTime']['input']>;
  hubPort: Scalars['Port']['input'];
};

export type CreateDevicePortFromDevice = {
  devicePort: Scalars['Port']['input'];
  expiration?: InputMaybe<Scalars['DateTime']['input']>;
  hubPort: Scalars['Port']['input'];
};

export type CreateStaticIp = {
  deviceId?: InputMaybe<Scalars['ID']['input']>;
  expiration?: InputMaybe<Scalars['DateTime']['input']>;
  ip: Scalars['IPv4']['input'];
};

export type UpdateAppUser = {
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  state?: InputMaybe<App_User_State>;
  username?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateDevice = {
  hasInternetAccess?: InputMaybe<Scalars['Boolean']['input']>;
  internetAccessExpiration?: InputMaybe<Scalars['DateTime']['input']>;
  ip?: InputMaybe<Scalars['IPv4']['input']>;
  isAllowed?: InputMaybe<Scalars['Boolean']['input']>;
  isKnown?: InputMaybe<Scalars['Boolean']['input']>;
  isKnownExpiration?: InputMaybe<Scalars['DateTime']['input']>;
  mac?: InputMaybe<Scalars['MAC']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  ownerId?: InputMaybe<Scalars['ID']['input']>;
  staticIpId?: InputMaybe<Scalars['ID']['input']>;
  wifiName?: InputMaybe<Scalars['String']['input']>;
  wifiPass?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateDeviceGroup = {
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  ownerId?: InputMaybe<Scalars['ID']['input']>;
};

export type UpdateDevicePhoto = {
  displayOrder?: InputMaybe<Scalars['Int']['input']>;
};

export type UpdateDevicePort = {
  deviceId?: InputMaybe<Scalars['ID']['input']>;
  devicePort?: InputMaybe<Scalars['Port']['input']>;
  expiration?: InputMaybe<Scalars['DateTime']['input']>;
  hubPort?: InputMaybe<Scalars['Port']['input']>;
};

export type UpdateStaticIp = {
  deviceId?: InputMaybe<Scalars['ID']['input']>;
  expiration?: InputMaybe<Scalars['DateTime']['input']>;
  ip?: InputMaybe<Scalars['IPv4']['input']>;
};

export type UpdateUserNotification = {
  isShown?: InputMaybe<Scalars['Boolean']['input']>;
};

export type DeviceModalQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type DeviceModalQuery = { __typename?: 'Query', appUser: { __typename?: 'AppUser', deviceList: Array<{ __typename?: 'Device', id: string, name: string, mac: any, ip?: any | null, isKnown: boolean, isKnownExpiration?: any | null, hasInternetAccess: boolean, internetAccessExpiration?: any | null, wifiName?: string | null, wifiPass?: string | null, isAllowed: boolean, createdAt: any, updatedAt?: any | null, deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string, name: string, isActive: boolean, createdAt: any, updatedAt?: any | null } | null>, devicePortList: Array<{ __typename?: 'DevicePort', id: string, hubPort: any, devicePort: any, expiration?: any | null, createdAt: any, updatedAt?: any | null } | null>, devicePhotoList: Array<{ __typename?: 'DevicePhoto', id: string, filename: string, displayOrder: number, createdAt: any, updatedAt?: any | null } | null>, staticIp?: { __typename?: 'StaticIp', id: string, ip: any, expiration?: any | null, createdAt: any, updatedAt?: any | null } | null }> } };

export type RemoveDevice2MutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveDevice2Mutation = { __typename?: 'Mutation', removeDevice: boolean };

export type DeviceListSelectQueryVariables = Exact<{
  filter: Device_Filter;
}>;


export type DeviceListSelectQuery = { __typename?: 'Query', deviceList: { __typename?: 'DeviceConnection', edges: Array<{ __typename?: 'DeviceEdge', node: { __typename?: 'Device', id: string, name: string, mac: any, ip?: any | null } }> } };

export type GroupListSelectQueryVariables = Exact<{
  filter: Device_Group_Filter;
}>;


export type GroupListSelectQuery = { __typename?: 'Query', deviceGroupList: { __typename?: 'DeviceGroupConnection', edges: Array<{ __typename?: 'DeviceGroupEdge', node: { __typename?: 'DeviceGroup', id: string, name: string } }> } };

export type StaticIpSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type StaticIpSelectQuery = { __typename?: 'Query', staticIpList: { __typename?: 'StaticIpConnection', edges: Array<{ __typename?: 'StaticIpEdge', node: { __typename?: 'StaticIp', id: string, ip: any, device?: { __typename?: 'Device', id: string } | null } }> } };

export type AppUserListSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type AppUserListSelectQuery = { __typename?: 'Query', appUserList: { __typename?: 'AppUserConnection', edges: Array<{ __typename?: 'AppUserEdge', node: { __typename?: 'AppUser', id: string, username: string, name: string } }> } };

export type DeviceGroupModalQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type DeviceGroupModalQuery = { __typename?: 'Query', appUser: { __typename?: 'AppUser', deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string, name: string, isActive: boolean, createdAt: any, updatedAt?: any | null, deviceList: Array<{ __typename?: 'Device', id: string, ip?: any | null, name: string, mac: any }> }> } };

export type RemoveDeviceGroup2MutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveDeviceGroup2Mutation = { __typename?: 'Mutation', removeDeviceGroup: boolean };

export type UpdateAppUserMutationVariables = Exact<{
  id: Scalars['ID']['input'];
  data: UpdateAppUser;
}>;


export type UpdateAppUserMutation = { __typename?: 'Mutation', updateAppUser: { __typename?: 'AppUser', id: string, username: string, name: string, isAdmin: boolean, state: App_User_State, roles: Array<string>, lastLoginAt?: any | null, createdAt: any, updatedAt?: any | null, deviceList: Array<{ __typename?: 'Device', id: string }>, deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string }> } };

export type RemoveAppUserMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveAppUserMutation = { __typename?: 'Mutation', removeAppUser: boolean };

export type CreateAppUserMutationVariables = Exact<{
  data: CreateAppUser;
}>;


export type CreateAppUserMutation = { __typename?: 'Mutation', createAppUser: { __typename?: 'AppUser', id: string, username: string, name: string, isAdmin: boolean, state: App_User_State, roles: Array<string>, lastLoginAt?: any | null, createdAt: any, updatedAt?: any | null, deviceList: Array<{ __typename?: 'Device', id: string }>, deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string }> } };

export type AppUserListQueryVariables = Exact<{ [key: string]: never; }>;


export type AppUserListQuery = { __typename?: 'Query', appUserList: { __typename?: 'AppUserConnection', edges: Array<{ __typename?: 'AppUserEdge', node: { __typename?: 'AppUser', id: string, username: string, name: string, isAdmin: boolean, state: App_User_State, roles: Array<string>, lastLoginAt?: any | null, createdAt: any, updatedAt?: any | null, deviceList: Array<{ __typename?: 'Device', id: string }>, deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string }> } }> } };

export type MeWithTokensQueryVariables = Exact<{ [key: string]: never; }>;


export type MeWithTokensQuery = { __typename?: 'Query', me: { __typename?: 'AppUserWithTokens', appUser: { __typename?: 'AppUser', name: string }, tokenList: Array<{ __typename?: 'RefreshToken', id: string, valid: any }> } };

export type RemoveRefreshTokenMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveRefreshTokenMutation = { __typename?: 'Mutation', removeRefreshToken: boolean };

export type DeviceListQueryVariables = Exact<{
  filter: Device_Filter;
  after?: InputMaybe<Scalars['Cursor']['input']>;
}>;


export type DeviceListQuery = { __typename?: 'Query', deviceList: { __typename?: 'DeviceConnection', edges: Array<{ __typename?: 'DeviceEdge', node: { __typename?: 'Device', id: string, name: string, mac: any, ip?: any | null, isKnown: boolean, isKnownExpiration?: any | null, hasInternetAccess: boolean, internetAccessExpiration?: any | null, wifiName?: string | null, wifiPass?: string | null, isAllowed: boolean, createdAt: any, updatedAt?: any | null, owner?: { __typename?: 'AppUser', id: string, username: string, name: string, isAdmin: boolean, state: App_User_State, roles: Array<string>, lastLoginAt?: any | null, createdAt: any, updatedAt?: any | null } | null, deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string, name: string } | null>, devicePortList: Array<{ __typename?: 'DevicePort', id: string, hubPort: any, devicePort: any, expiration?: any | null } | null>, devicePhotoList: Array<{ __typename?: 'DevicePhoto', id: string, filename: string } | null>, staticIp?: { __typename?: 'StaticIp', id: string, ip: any, expiration?: any | null } | null } }>, pageInfo: { __typename?: 'PageInfo', endCursor: string, hasNextPage: boolean } } };

export type RemoveDeviceMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveDeviceMutation = { __typename?: 'Mutation', removeDevice: boolean };

export type DeviceQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type DeviceQuery = { __typename?: 'Query', device: { __typename?: 'Device', name: string, mac: any, ip?: any | null, isKnown: boolean, isKnownExpiration?: any | null, hasInternetAccess: boolean, internetAccessExpiration?: any | null, wifiName?: string | null, wifiPass?: string | null, isAllowed: boolean, owner?: { __typename?: 'AppUser', id: string } | null, deviceGroupList: Array<{ __typename?: 'DeviceGroup', id: string } | null>, devicePortList: Array<{ __typename?: 'DevicePort', id: string, hubPort: any, devicePort: any, expiration?: any | null } | null>, devicePhotoList: Array<{ __typename?: 'DevicePhoto', id: string, filename: string } | null>, staticIp?: { __typename?: 'StaticIp', id: string } | null } };

export type CreateDeviceMutationVariables = Exact<{
  data: CreateDevice;
}>;


export type CreateDeviceMutation = { __typename?: 'Mutation', createDevice: { __typename?: 'Device', name: string } };

export type UpdateDeviceMutationVariables = Exact<{
  id: Scalars['ID']['input'];
  data: UpdateDevice;
}>;


export type UpdateDeviceMutation = { __typename?: 'Mutation', updateDevice: { __typename?: 'Device', name: string } };

export type AddDeviceToGroup2MutationVariables = Exact<{
  deviceId: Scalars['ID']['input'];
  groupId: Scalars['ID']['input'];
}>;


export type AddDeviceToGroup2Mutation = { __typename?: 'Mutation', addDeviceToGroup: boolean };

export type RemoveDeviceFromGroup2MutationVariables = Exact<{
  deviceId: Scalars['ID']['input'];
  groupId: Scalars['ID']['input'];
}>;


export type RemoveDeviceFromGroup2Mutation = { __typename?: 'Mutation', removeDeviceFromGroup: boolean };

export type CreateDevicePortMutationVariables = Exact<{
  devicePort: CreateDevicePort;
}>;


export type CreateDevicePortMutation = { __typename?: 'Mutation', createDevicePort: { __typename?: 'DevicePort', id: string } };

export type RemoveDevicePortMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveDevicePortMutation = { __typename?: 'Mutation', removeDevicePort: boolean };

export type CreateDevicePhotoMutationVariables = Exact<{
  devicePhoto: CreateDevicePhoto;
}>;


export type CreateDevicePhotoMutation = { __typename?: 'Mutation', createDevicePhoto: { __typename?: 'DevicePhoto', id: string, filename: string } };

export type RemoveDevicePhotoMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveDevicePhotoMutation = { __typename?: 'Mutation', removeDevicePhoto: boolean };

export type DeviceGroupListQueryVariables = Exact<{
  filter: Device_Group_Filter;
  after?: InputMaybe<Scalars['Cursor']['input']>;
}>;


export type DeviceGroupListQuery = { __typename?: 'Query', deviceGroupList: { __typename?: 'DeviceGroupConnection', edges: Array<{ __typename?: 'DeviceGroupEdge', node: { __typename?: 'DeviceGroup', id: string, name: string, isActive: boolean, createdAt: any, updatedAt?: any | null, deviceList: Array<{ __typename?: 'Device', id: string, name: string, ip?: any | null, mac: any }>, owner?: { __typename?: 'AppUser', id: string, name: string } | null } }>, pageInfo: { __typename?: 'PageInfo', endCursor: string, hasNextPage: boolean } } };

export type RemoveDeviceGroupMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveDeviceGroupMutation = { __typename?: 'Mutation', removeDeviceGroup: boolean };

export type DeviceGroupQueryVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type DeviceGroupQuery = { __typename?: 'Query', deviceGroup: { __typename?: 'DeviceGroup', id: string, name: string, isActive: boolean, deviceList: Array<{ __typename?: 'Device', id: string }>, owner?: { __typename?: 'AppUser', id: string } | null } };

export type CreateGroupMutationVariables = Exact<{
  data: CreateDeviceGroup;
}>;


export type CreateGroupMutation = { __typename?: 'Mutation', createDeviceGroup: { __typename?: 'DeviceGroup', name: string } };

export type UpdateGroupMutationVariables = Exact<{
  id: Scalars['ID']['input'];
  data: UpdateDeviceGroup;
}>;


export type UpdateGroupMutation = { __typename?: 'Mutation', updateDeviceGroup: { __typename?: 'DeviceGroup', name: string } };

export type AddDeviceToGroup1MutationVariables = Exact<{
  deviceId: Scalars['ID']['input'];
  groupId: Scalars['ID']['input'];
}>;


export type AddDeviceToGroup1Mutation = { __typename?: 'Mutation', addDeviceToGroup: boolean };

export type RemoveDeviceFromGroup1MutationVariables = Exact<{
  deviceId: Scalars['ID']['input'];
  groupId: Scalars['ID']['input'];
}>;


export type RemoveDeviceFromGroup1Mutation = { __typename?: 'Mutation', removeDeviceFromGroup: boolean };

export type UpdateStaticIpMutationVariables = Exact<{
  id: Scalars['ID']['input'];
  data: UpdateStaticIp;
}>;


export type UpdateStaticIpMutation = { __typename?: 'Mutation', updateStaticIp: { __typename?: 'StaticIp', id: string, ip: any, expiration?: any | null, createdAt: any, updatedAt?: any | null, device?: { __typename?: 'Device', id: string, name: string, ip?: any | null } | null } };

export type RemoveStaticIpMutationVariables = Exact<{
  id: Scalars['ID']['input'];
}>;


export type RemoveStaticIpMutation = { __typename?: 'Mutation', removeStaticIp: boolean };

export type CreateStaticIpMutationVariables = Exact<{
  data: CreateStaticIp;
}>;


export type CreateStaticIpMutation = { __typename?: 'Mutation', createStaticIp: { __typename?: 'StaticIp', id: string, ip: any, expiration?: any | null, createdAt: any, updatedAt?: any | null, device?: { __typename?: 'Device', id: string, name: string, ip?: any | null } | null } };

export type StaticIpListQueryVariables = Exact<{ [key: string]: never; }>;


export type StaticIpListQuery = { __typename?: 'Query', staticIpList: { __typename?: 'StaticIpConnection', edges: Array<{ __typename?: 'StaticIpEdge', node: { __typename?: 'StaticIp', id: string, ip: any, expiration?: any | null, createdAt: any, updatedAt?: any | null, device?: { __typename?: 'Device', id: string, name: string, ip?: any | null } | null } }> } };


export const DeviceModalDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"deviceModal"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appUser"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"mac"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"isKnown"}},{"kind":"Field","name":{"kind":"Name","value":"isKnownExpiration"}},{"kind":"Field","name":{"kind":"Name","value":"hasInternetAccess"}},{"kind":"Field","name":{"kind":"Name","value":"internetAccessExpiration"}},{"kind":"Field","name":{"kind":"Name","value":"wifiName"}},{"kind":"Field","name":{"kind":"Name","value":"wifiPass"}},{"kind":"Field","name":{"kind":"Name","value":"isAllowed"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"devicePortList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hubPort"}},{"kind":"Field","name":{"kind":"Name","value":"devicePort"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"devicePhotoList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"filename"}},{"kind":"Field","name":{"kind":"Name","value":"displayOrder"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"staticIp"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]}}]}}]} as unknown as DocumentNode<DeviceModalQuery, DeviceModalQueryVariables>;
export const RemoveDevice2Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDevice2"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDevice"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveDevice2Mutation, RemoveDevice2MutationVariables>;
export const DeviceListSelectDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"deviceListSelect"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filter"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"DEVICE_FILTER"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"10000"}},{"kind":"Argument","name":{"kind":"Name","value":"filter"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filter"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"mac"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}}]}}]}}]}}]}}]} as unknown as DocumentNode<DeviceListSelectQuery, DeviceListSelectQueryVariables>;
export const GroupListSelectDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"groupListSelect"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filter"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"DEVICE_GROUP_FILTER"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"1000"}},{"kind":"Argument","name":{"kind":"Name","value":"filter"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filter"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]}}]} as unknown as DocumentNode<GroupListSelectQuery, GroupListSelectQueryVariables>;
export const StaticIpSelectDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"staticIpSelect"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"staticIpList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"10000"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"device"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<StaticIpSelectQuery, StaticIpSelectQueryVariables>;
export const AppUserListSelectDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"appUserListSelect"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appUserList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"10000"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]}}]} as unknown as DocumentNode<AppUserListSelectQuery, AppUserListSelectQueryVariables>;
export const DeviceGroupModalDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"deviceGroupModal"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appUser"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"mac"}}]}}]}}]}}]}}]} as unknown as DocumentNode<DeviceGroupModalQuery, DeviceGroupModalQueryVariables>;
export const RemoveDeviceGroup2Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDeviceGroup2"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDeviceGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveDeviceGroup2Mutation, RemoveDeviceGroup2MutationVariables>;
export const UpdateAppUserDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateAppUser"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"updateAppUser"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateAppUser"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"appUser"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isAdmin"}},{"kind":"Field","name":{"kind":"Name","value":"state"}},{"kind":"Field","name":{"kind":"Name","value":"roles"}},{"kind":"Field","name":{"kind":"Name","value":"lastLoginAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode<UpdateAppUserMutation, UpdateAppUserMutationVariables>;
export const RemoveAppUserDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeAppUser"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeAppUser"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveAppUserMutation, RemoveAppUserMutationVariables>;
export const CreateAppUserDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createAppUser"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"createAppUser"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createAppUser"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"appUser"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isAdmin"}},{"kind":"Field","name":{"kind":"Name","value":"state"}},{"kind":"Field","name":{"kind":"Name","value":"roles"}},{"kind":"Field","name":{"kind":"Name","value":"lastLoginAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode<CreateAppUserMutation, CreateAppUserMutationVariables>;
export const AppUserListDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"appUserList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appUserList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"10000"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isAdmin"}},{"kind":"Field","name":{"kind":"Name","value":"state"}},{"kind":"Field","name":{"kind":"Name","value":"roles"}},{"kind":"Field","name":{"kind":"Name","value":"lastLoginAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<AppUserListQuery, AppUserListQueryVariables>;
export const MeWithTokensDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"meWithTokens"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"appUser"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"tokenList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"valid"}}]}}]}}]}}]} as unknown as DocumentNode<MeWithTokensQuery, MeWithTokensQueryVariables>;
export const RemoveRefreshTokenDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeRefreshToken"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeRefreshToken"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveRefreshTokenMutation, RemoveRefreshTokenMutationVariables>;
export const DeviceListDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"deviceList"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filter"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"DEVICE_FILTER"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"after"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Cursor"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"6"}},{"kind":"Argument","name":{"kind":"Name","value":"after"},"value":{"kind":"Variable","name":{"kind":"Name","value":"after"}}},{"kind":"Argument","name":{"kind":"Name","value":"filter"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filter"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"mac"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"isKnown"}},{"kind":"Field","name":{"kind":"Name","value":"isKnownExpiration"}},{"kind":"Field","name":{"kind":"Name","value":"hasInternetAccess"}},{"kind":"Field","name":{"kind":"Name","value":"internetAccessExpiration"}},{"kind":"Field","name":{"kind":"Name","value":"wifiName"}},{"kind":"Field","name":{"kind":"Name","value":"wifiPass"}},{"kind":"Field","name":{"kind":"Name","value":"isAllowed"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}},{"kind":"Field","name":{"kind":"Name","value":"owner"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"username"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"isAdmin"}},{"kind":"Field","name":{"kind":"Name","value":"state"}},{"kind":"Field","name":{"kind":"Name","value":"roles"}},{"kind":"Field","name":{"kind":"Name","value":"lastLoginAt"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}},{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"devicePortList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hubPort"}},{"kind":"Field","name":{"kind":"Name","value":"devicePort"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}}]}},{"kind":"Field","name":{"kind":"Name","value":"devicePhotoList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"filename"}}]}},{"kind":"Field","name":{"kind":"Name","value":"staticIp"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"pageInfo"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"endCursor"}},{"kind":"Field","name":{"kind":"Name","value":"hasNextPage"}}]}}]}}]}}]} as unknown as DocumentNode<DeviceListQuery, DeviceListQueryVariables>;
export const RemoveDeviceDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDevice"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDevice"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveDeviceMutation, RemoveDeviceMutationVariables>;
export const DeviceDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"device"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"device"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"mac"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"isKnown"}},{"kind":"Field","name":{"kind":"Name","value":"isKnownExpiration"}},{"kind":"Field","name":{"kind":"Name","value":"hasInternetAccess"}},{"kind":"Field","name":{"kind":"Name","value":"internetAccessExpiration"}},{"kind":"Field","name":{"kind":"Name","value":"wifiName"}},{"kind":"Field","name":{"kind":"Name","value":"wifiPass"}},{"kind":"Field","name":{"kind":"Name","value":"isAllowed"}},{"kind":"Field","name":{"kind":"Name","value":"owner"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"devicePortList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"hubPort"}},{"kind":"Field","name":{"kind":"Name","value":"devicePort"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}}]}},{"kind":"Field","name":{"kind":"Name","value":"devicePhotoList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"filename"}}]}},{"kind":"Field","name":{"kind":"Name","value":"staticIp"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]} as unknown as DocumentNode<DeviceQuery, DeviceQueryVariables>;
export const CreateDeviceDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createDevice"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"createDevice"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createDevice"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"device"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<CreateDeviceMutation, CreateDeviceMutationVariables>;
export const UpdateDeviceDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateDevice"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"updateDevice"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateDevice"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"device"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<UpdateDeviceMutation, UpdateDeviceMutationVariables>;
export const AddDeviceToGroup2Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"addDeviceToGroup2"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"addDeviceToGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"deviceId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}}},{"kind":"Argument","name":{"kind":"Name","value":"groupId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}}}]}]}}]} as unknown as DocumentNode<AddDeviceToGroup2Mutation, AddDeviceToGroup2MutationVariables>;
export const RemoveDeviceFromGroup2Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDeviceFromGroup2"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDeviceFromGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"deviceId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}}},{"kind":"Argument","name":{"kind":"Name","value":"groupId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}}}]}]}}]} as unknown as DocumentNode<RemoveDeviceFromGroup2Mutation, RemoveDeviceFromGroup2MutationVariables>;
export const CreateDevicePortDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createDevicePort"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"devicePort"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"createDevicePort"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createDevicePort"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"devicePort"},"value":{"kind":"Variable","name":{"kind":"Name","value":"devicePort"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<CreateDevicePortMutation, CreateDevicePortMutationVariables>;
export const RemoveDevicePortDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDevicePort"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDevicePort"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveDevicePortMutation, RemoveDevicePortMutationVariables>;
export const CreateDevicePhotoDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createDevicePhoto"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"devicePhoto"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"createDevicePhoto"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createDevicePhoto"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"devicePhoto"},"value":{"kind":"Variable","name":{"kind":"Name","value":"devicePhoto"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"filename"}}]}}]}}]} as unknown as DocumentNode<CreateDevicePhotoMutation, CreateDevicePhotoMutationVariables>;
export const RemoveDevicePhotoDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDevicePhoto"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDevicePhoto"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveDevicePhotoMutation, RemoveDevicePhotoMutationVariables>;
export const DeviceGroupListDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"deviceGroupList"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"filter"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"DEVICE_GROUP_FILTER"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"after"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Cursor"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceGroupList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"9"}},{"kind":"Argument","name":{"kind":"Name","value":"after"},"value":{"kind":"Variable","name":{"kind":"Name","value":"after"}}},{"kind":"Argument","name":{"kind":"Name","value":"filter"},"value":{"kind":"Variable","name":{"kind":"Name","value":"filter"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"mac"}}]}},{"kind":"Field","name":{"kind":"Name","value":"owner"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"pageInfo"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"endCursor"}},{"kind":"Field","name":{"kind":"Name","value":"hasNextPage"}}]}}]}}]}}]} as unknown as DocumentNode<DeviceGroupListQuery, DeviceGroupListQueryVariables>;
export const RemoveDeviceGroupDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDeviceGroup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDeviceGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveDeviceGroupMutation, RemoveDeviceGroupMutationVariables>;
export const DeviceGroupDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"deviceGroup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"deviceGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"deviceList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"owner"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isActive"}}]}}]}}]} as unknown as DocumentNode<DeviceGroupQuery, DeviceGroupQueryVariables>;
export const CreateGroupDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createGroup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"createDeviceGroup"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createDeviceGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"deviceGroup"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<CreateGroupMutation, CreateGroupMutationVariables>;
export const UpdateGroupDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateGroup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"updateDeviceGroup"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateDeviceGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"deviceGroup"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]} as unknown as DocumentNode<UpdateGroupMutation, UpdateGroupMutationVariables>;
export const AddDeviceToGroup1Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"addDeviceToGroup1"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"addDeviceToGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"deviceId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}}},{"kind":"Argument","name":{"kind":"Name","value":"groupId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}}}]}]}}]} as unknown as DocumentNode<AddDeviceToGroup1Mutation, AddDeviceToGroup1MutationVariables>;
export const RemoveDeviceFromGroup1Document = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeDeviceFromGroup1"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeDeviceFromGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"deviceId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"deviceId"}}},{"kind":"Argument","name":{"kind":"Name","value":"groupId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}}}]}]}}]} as unknown as DocumentNode<RemoveDeviceFromGroup1Mutation, RemoveDeviceFromGroup1MutationVariables>;
export const UpdateStaticIpDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"updateStaticIp"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"updateStaticIp"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateStaticIp"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}},{"kind":"Argument","name":{"kind":"Name","value":"staticIp"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"device"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<UpdateStaticIpMutation, UpdateStaticIpMutationVariables>;
export const RemoveStaticIpDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"removeStaticIp"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"removeStaticIp"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}]}}]} as unknown as DocumentNode<RemoveStaticIpMutation, RemoveStaticIpMutationVariables>;
export const CreateStaticIpDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createStaticIp"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"data"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"createStaticIp"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createStaticIp"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"staticIp"},"value":{"kind":"Variable","name":{"kind":"Name","value":"data"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"device"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]} as unknown as DocumentNode<CreateStaticIpMutation, CreateStaticIpMutationVariables>;
export const StaticIpListDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"StaticIpList"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"staticIpList"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"10000"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"device"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"ip"}}]}},{"kind":"Field","name":{"kind":"Name","value":"ip"}},{"kind":"Field","name":{"kind":"Name","value":"expiration"}},{"kind":"Field","name":{"kind":"Name","value":"createdAt"}},{"kind":"Field","name":{"kind":"Name","value":"updatedAt"}}]}}]}}]}}]}}]} as unknown as DocumentNode<StaticIpListQuery, StaticIpListQueryVariables>;