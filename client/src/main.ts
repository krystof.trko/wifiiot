import { createApp, provide, h } from 'vue'
import App from './App.vue'
import router from './router'

import { IonicVue } from '@ionic/vue'

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css'
import '@ionic/vue/css/structure.css'
import '@ionic/vue/css/typography.css'

/* Ionic aptional */
import '@ionic/vue/css/padding.css'
import '@ionic/vue/css/float-elements.css'
import '@ionic/vue/css/text-alignment.css'
import '@ionic/vue/css/text-transformation.css'
import '@ionic/vue/css/flex-utils.css'
import '@ionic/vue/css/display.css'

import '@ionic/vue/css/palettes/dark.class.css'

/* Theme variables */
import './theme/variables.css'
import { createPinia } from 'pinia'

import { DefaultApolloClient } from '@vue/apollo-composable'
import { userAuthService, apolloClient } from './boot/api'
import { useUserStore } from './stores/userStore'
import { useGlobalStore } from './stores/globalStore'
import consoleArt from './boot/consoleArt'

consoleArt()

const pinia = createPinia()

const app = createApp({
    setup() {
        provide(DefaultApolloClient, apolloClient),
            provide('userAuthService', userAuthService)
    },
    render: () => h(App),
})

app.use(pinia)
// Inicializace store pro přihlašovbací tokeny
await useUserStore().init()
// Načíst dark mode
useGlobalStore().init()

app.use(IonicVue).use(router)

router.isReady().then(() => {
    app.mount('#app')
})
