import config from '@/config/config'
import { Storage } from '@ionic/storage'

export default class UserAuthService {
    private static readonly jwt = 'JWT'
    private static readonly refresh = 'refresh'

    constructor(private storage: Storage) {}

    /**
     * Pokud má uživaztel uloženy oba tokeny
     */
    public async hasToken(): Promise<boolean> {
        const jwt = await this.storage.get(UserAuthService.jwt)
        const refresh = await this.storage.get(UserAuthService.refresh)

        return jwt !== null && refresh !== null
    }

    /**
     * Získat JWT
     */
    public async logIn(data: any): Promise<void | [any, any]> {
        return fetch(config.uri.login, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then((response) => {
                if (!response.ok || response.status !== 200) {
                    throw new Error()
                }
                return response.json()
            })
            .then((data) => {
                const set1 = this.storage.set(UserAuthService.jwt, data.token)
                const set2 = this.storage.set(
                    UserAuthService.refresh,
                    data.refresh_token
                )

                return Promise.all([set1, set2])
            })
    }

    /**
     * Obnovit access token pomocí renew tokenu, navrátí access token
     */
    public async refreshToken(): Promise<string> {
        const refresh_token = await this.storage.get(UserAuthService.refresh)

        if (!refresh_token) {
            return new Promise((resolve, reject) => reject('Invalid token'))
        }

        return fetch(config.uri.refresh, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ refresh_token }),
        })
            .then((response) => {
                if (!response.ok || response.status !== 200) {
                    throw new Error()
                }
                return response.json()
            })
            .then((data) => {
                const set1 = this.storage.set(UserAuthService.jwt, data.token)
                const set2 = this.storage.set(
                    UserAuthService.refresh,
                    data.refresh_token
                )

                return Promise.all([set1, set2]).then(() => data.token)
            })
    }

    /**
     * Smazat uložená data a refresh token
     */
    public logOut(): Promise<[any, any, void]> {
        return Promise.all([
            this.storage.remove(UserAuthService.jwt),
            this.storage.remove(UserAuthService.refresh),
            this.storage.clear(),
        ])
    }

    public async getHttpToken(): Promise<string> {
        const token = await this.storage.get(UserAuthService.jwt)
        return `Bearer ${token}`
    }
}
