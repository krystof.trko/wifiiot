import {
    ApolloClient,
    InMemoryCache,
    ApolloLink,
    from,
    fromPromise,
    Observable,
} from '@apollo/client/core'
import { onError } from '@apollo/client/link/error'
import config from '@/config/config'
import { Storage } from '@ionic/storage'
import toast from '@/helpers/toast'
import UserAuthService from '@/services/UserAuthService'
import router from '@/router'
import { Route } from '@/router/route'
import { useUserStore } from '@/stores/userStore'
import type { ServerError } from '@apollo/client/link/utils/index.js'
import { createUploadLink } from 'apollo-upload-client'
import { relayStylePagination } from '@apollo/client/utilities'

const storage = await new Storage().create()
const userAuthService = new UserAuthService(storage)

const authLink = new ApolloLink((operation, forward) => {
    return fromPromise(
        userAuthService.getHttpToken().then((token) => {
            operation.setContext(({ headers = {} }) => ({
                headers: {
                    ...headers,
                    authorization: token,
                },
            }))

            return true
        })
    ).flatMap(() => forward(operation))
})

let isRefreshing = false
let pendingRequests: (() => void)[] = []

const resolvePendingRequests = () => {
    pendingRequests.map((callback) => callback())
    pendingRequests = []
}

const errorLink = onError(
    ({ graphQLErrors, networkError, operation, forward }) => {
        if (graphQLErrors) {
            graphQLErrors.forEach(({ message }) => toast(message, 'danger'))
        }

        if ((networkError as ServerError)?.statusCode === 401) {
            let forwardNext: Observable<unknown>

            if (isRefreshing) {
                // Pokud probíhá refresh, pozdržet další požadavky
                forwardNext = fromPromise(
                    new Promise((resolve) => {
                        // Čekající requesty
                        pendingRequests.push(() => resolve)
                    })
                )
            } else {
                isRefreshing = true

                forwardNext = fromPromise(
                    userAuthService
                        .refreshToken()
                        .then(() => {
                            resolvePendingRequests()
                            return true
                        })
                        .catch(() => {
                            toast('Přihlášení vypršelo', 'warning')
                            useUserStore()
                                .logOut()
                                .then(() => {
                                    router.push({ name: Route.login })
                                })
                        })
                        .finally(() => (isRefreshing = false))
                ).filter((value) => Boolean(value))
            }

            // Navrátit přesměrování
            return forwardNext.flatMap(() => forward(operation))
        }

        if (networkError) {
            toast('Chyba sítě: ' + networkError, 'danger')
        }
    }
)

const upload = createUploadLink({
    uri: config.uri.api,
})

const cache = new InMemoryCache({
    typePolicies: {
        Query: {
            fields: {
                deviceList: relayStylePagination(),
                deviceGroupList: relayStylePagination(),
            },
        },
    },
})

const apolloClient = new ApolloClient({
    link: from([errorLink, authLink, upload]),
    cache,
})

export { userAuthService, apolloClient }
